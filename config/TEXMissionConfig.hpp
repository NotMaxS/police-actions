class MissionConfig
{
    #include "missionConfig\AmbientAnimations.hpp"
    #include "missionConfig\FlashAI.hpp"
    #include "missionConfig\FlashLauncher.hpp"
    #include "missionConfig\Score.hpp"
    #include "missionConfig\VehicleComposition.hpp"
    #include "missionConfig\VehicleTextures.hpp"
};
