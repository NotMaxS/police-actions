// XPTCuratorLoadouts.hpp
// Used for defining advanced loadouts for Zeus spawned units (works almost the same as XPTLoadouts)
// Use XPT_fnc_exportInventory to get correct loadout format, picture guide can be found here: https://imgur.com/a/GrDaJNZ, courtesy of O'Mally
// Supports applying loadouts to crew in vehicles as long as crew classname matches the one defined in here
// Default behaviour is to check if the Zeus placed unit has a special loadout defined. Otherwise, it will use the default loadout as normal
// Advanced functionality allows mission creator to define exactly where items are placed in unit inventories
// Also supports sub-loadout randomization. If a loadout has sub-classes defined, the script will automatically select one of them to apply to the unit.
class curatorLoadouts
{	

	#define CHDKZ_UNIFORM "CUP_U_O_CHDKZ_Kam_02", "CUP_U_O_CHDKZ_Kam_07", "CUP_U_O_CHDKZ_Kam_05", "CUP_U_O_CHDKZ_Kam_06", "CUP_U_O_CHDKZ_Bardak", "CUP_U_O_CHDKZ_Lopotev", "CUP_U_C_Citizen_04"
	#define CHDKZ_HEADGEAR "CUP_H_PMC_Beanie_Khaki", "CUP_H_FR_BandanaGreen", "CUP_H_C_Beanie_02", "CUP_H_ChDKZ_Beanie"
	#define CHDKZ_FACEWEAR "G_Bandanna_Syndikat2", "G_Bandanna_RedFlame1", "G_Balaclava_blk_lxWS", "G_Bandanna_Skull2", "G_Balaclava_blk", "G_Balaclava_Skull1", "G_Balaclava_Scarecrow_01", "G_Balaclava_Flames1", "tmtm_f_balaclava_slit_loose_blue", "tmtm_f_balaclava_slit_brown", "tmtm_f_balaclava_slit_burgundy", "tmtm_f_balaclava_slit_reedw", "tmtm_f_balaclava_slit_grayred"
	#define CHDKZ_UNIFORM_MELEE "CUP_U_C_Woodlander_03", "CUP_U_C_Woodlander_01", "CUP_U_C_Woodlander_02", "CUP_U_C_Woodlander_04"

	#define CIV_UNIFORM "CUP_U_C_Woodlander_03", "CUP_U_C_Woodlander_01", "CUP_U_C_Woodlander_02", "CUP_U_C_Woodlander_04", "CUP_U_C_Worker_03", "CUP_U_C_Worker_02", "CUP_U_O_CHDKZ_Bardak", "CUP_U_O_CHDKZ_Lopotev", "CUP_U_C_Citizen_03", "CUP_U_C_Citizen_01", "CUP_U_C_Citizen_02"
	#define CIV_HEADGEAR "CUP_H_FR_BandanaGreen", "CUP_H_SLA_BeanieGreen", "CUP_H_C_Beanie_02", "CUP_H_PMC_Cap_Grey", "H_Bandanna_gry", "CUP_H_FR_BandanaWdl", "H_Hat_grey", "H_Hat_brown", ""

	class CUP_O_INS_Commander {
		displayName = "CUP_O_INS_Commander";

		primaryWeapon[] = {"CUP_arifle_AKS74U","CUP_muzzle_mfsup_Flashhider_545x39_Black","","",{"CUP_30Rnd_545x39_AK74M_M",30},{},""};
		secondaryWeapon[] = {};
		handgunWeapon[] = {};

		uniformClass[] = {CHDKZ_UNIFORM}; 
		headgearClass[] = {CHDKZ_HEADGEAR}; 
		facewearClass[] = {CHDKZ_FACEWEAR}; 
		vestClass = "CUP_V_I_Carrier_Belt";

		linkedItems[] = {"ItemMap","","","ItemCompass","ItemWatch",""};

		uniformItems[] = {};
		vestItems[] = {{"CUP_30Rnd_545x39_AK74M_M",8,30},{"MiniGrenade",2,1}};

		basicMedUniform[] = {{"ACE_fieldDressing",8},{"ACE_morphine",2},{"ACE_epinephrine",1},{"ACE_tourniquet",1},{"ACE_splint",1}};
		basicMedVest[] = {};
		advMedUniform[] = {{"ACE_fieldDressing",8},{"ACE_morphine",2},{"ACE_epinephrine",1},{"ACE_tourniquet",1},{"ACE_splint",1}};

	};

	class CUP_O_INS_Soldier_MG {
		displayName = "CUP_O_INS_Soldier_MG";
		class subclassMG1
		{
			primaryWeapon[] = {"CUP_smg_SA61_RIS","","acc_flashlight_pistol","",{"CUP_50Rnd_B_765x17_Ball_M",50},{},""};
			secondaryWeapon[] = {};
			handgunWeapon[] = {};

			uniformClass[] = {CHDKZ_UNIFORM}; 
			headgearClass[] = {CHDKZ_HEADGEAR}; 
			facewearClass[] = {CHDKZ_FACEWEAR};
			vestClass = "CUP_V_I_Carrier_Belt";

			linkedItems[] = {"ItemMap","","","ItemCompass","ItemWatch",""};

			uniformItems[] = {};
			vestItems[] = {{"MiniGrenade",2,1},{"SmokeShell",2,1},{"CUP_50Rnd_B_765x17_Ball_M",6,50}};

			basicMedUniform[] = {{"ACE_fieldDressing",8},{"ACE_morphine",2},{"ACE_epinephrine",1},{"ACE_tourniquet",1},{"ACE_splint",1}};
			basicMedVest[] = {};
			advMedUniform[] = {{"ACE_fieldDressing",8},{"ACE_morphine",2},{"ACE_epinephrine",1},{"ACE_tourniquet",1},{"ACE_splint",1}};

		}; 

		class subclassMG2 
		{
			primaryWeapon[] = {"CUP_smg_Mac10","","","",{"CUP_30Rnd_45ACP_MAC10_M",30},{},""};
			secondaryWeapon[] = {};
			handgunWeapon[] = {};

			uniformClass[] = {CHDKZ_UNIFORM}; 
			headgearClass[] = {CHDKZ_HEADGEAR}; 
			facewearClass[] = {CHDKZ_FACEWEAR};
			vestClass = "CUP_V_I_Carrier_Belt";

			linkedItems[] = {"ItemMap","","","ItemCompass","ItemWatch",""};

			uniformItems[] = {};
			vestItems[] = {{"MiniGrenade",2,1},{"SmokeShell",2,1},{"CUP_30Rnd_45ACP_MAC10_M",8,30}};

			basicMedUniform[] = {{"ACE_fieldDressing",8},{"ACE_morphine",2},{"ACE_epinephrine",1},{"ACE_tourniquet",1},{"ACE_splint",1}};
			basicMedVest[] = {};
			advMedUniform[] = {{"ACE_fieldDressing",8},{"ACE_morphine",2},{"ACE_epinephrine",1},{"ACE_tourniquet",1},{"ACE_splint",1}};

		}; 
	};

	
	class CUP_O_INS_Soldier_AT {
		displayName = "CUP_O_INS_Soldier_AT";
		class subclassAT1
		{
			primaryWeapon[] = {"CUP_arifle_AKS","CUP_muzzle_mfsup_Flashhider_762x39_Black","CUP_acc_Flashlight","",{"CUP_30Rnd_762x39_AK47_bakelite_M",30},{},""};
			secondaryWeapon[] = {"CUP_launch_RPG18_Loaded","","","",{"CUP_RPG18_M",1},{},""};
			handgunWeapon[] = {};

			uniformClass[] = {CHDKZ_UNIFORM}; 
			headgearClass[] = {CHDKZ_HEADGEAR}; 
			facewearClass[] = {CHDKZ_FACEWEAR};
			vestClass = "CUP_V_I_Carrier_Belt";

			linkedItems[] = {"ItemMap","","","ItemCompass","ItemWatch",""};

			uniformItems[] = {};
			vestItems[] = {{"MiniGrenade",2,1},{"SmokeShell",2,1},{"CUP_30Rnd_762x39_AK47_bakelite_M",6,30}};

			basicMedUniform[] = {{"ACE_fieldDressing",8},{"ACE_morphine",2},{"ACE_epinephrine",1},{"ACE_tourniquet",1},{"ACE_splint",1}};
			basicMedVest[] = {};
			advMedUniform[] = {{"ACE_fieldDressing",8},{"ACE_morphine",2},{"ACE_epinephrine",1},{"ACE_tourniquet",1},{"ACE_splint",1}};

		}; 

		class sunbclassAT2 
		{
			primaryWeapon[] = {"CUP_SKS","","","",{"CUP_10Rnd_762x39_SKS_M",10},{},""};
			secondaryWeapon[] = {};
			handgunWeapon[] = {};

			uniformClass[] = {CHDKZ_UNIFORM}; 
			headgearClass[] = {CHDKZ_HEADGEAR}; 
			facewearClass[] = {CHDKZ_FACEWEAR};
			vestClass = "CUP_V_I_Carrier_Belt";

			linkedItems[] = {"ItemMap","","","ItemCompass","ItemWatch",""};

			uniformItems[] = {};
			vestItems[] = {{"CUP_10Rnd_762x39_SKS_M",10,10}};

			basicMedUniform[] = {{"ACE_fieldDressing",8},{"ACE_morphine",2},{"ACE_epinephrine",1},{"ACE_tourniquet",1},{"ACE_splint",1}};
			basicMedVest[] = {};
			advMedUniform[] = {{"ACE_fieldDressing",8},{"ACE_morphine",2},{"ACE_epinephrine",1},{"ACE_tourniquet",1},{"ACE_splint",1}};
		
		}; 
	};

	class CUP_O_INS_Soldier_AR {
		displayName = "CUP_O_INS_Soldier_AR";
		class subclassAR1 
		{
			primaryWeapon[] = {"CUP_lmg_MG3","","","",{"CUP_120Rnd_TE4_LRT4_Red_Tracer_762x51_Belt_M",120},{},""};
			secondaryWeapon[] = {};
			handgunWeapon[] = {};

			uniformClass[] = {CHDKZ_UNIFORM}; 
			headgearClass[] = {CHDKZ_HEADGEAR}; 
			facewearClass[] = {CHDKZ_FACEWEAR};
			vestClass = "CUP_V_O_Ins_Carrier_Rig";
			backpackClass = "CUP_B_INS_Backpack_AR";

			linkedItems[] = {"ItemMap","","","ItemCompass","ItemWatch",""};

			uniformItems[] = {};
			vestItems[] = {};
			backpackItems[] = {{"CUP_120Rnd_TE4_LRT4_Red_Tracer_762x51_Belt_M",5,120}};

			basicMedUniform[] = {{"ACE_fieldDressing",8},{"ACE_epinephrine",1},{"ACE_morphine",2},{"ACE_tourniquet",1},{"ACE_splint",1}};
			basicMedVest[] = {};
			basicMedBackpack[] = {};
			advMedUniform[] = {{"ACE_fieldDressing",8},{"ACE_morphine",2},{"ACE_epinephrine",1},{"ACE_tourniquet",1},{"ACE_splint",1}};
		
		}; 

		class subclassAR2 
		{
			primaryWeapon[] = {"sgun_HunterShotgun_01_sawedoff_F","","","",{"2Rnd_12Gauge_Pellets",2},{},""};
			secondaryWeapon[] = {};
			handgunWeapon[] = {};

			uniformClass[] = {CHDKZ_UNIFORM}; 
			headgearClass[] = {CHDKZ_HEADGEAR}; 
			facewearClass[] = {CHDKZ_FACEWEAR};
			vestClass = "CUP_V_I_Carrier_Belt";

			linkedItems[] = {"ItemMap","","","ItemCompass","ItemWatch",""};

			uniformItems[] = {};
			vestItems[] = {{"ACE_2Rnd_12Gauge_Pellets_No0_Buck",18,2}};

			basicMedUniform[] = {{"ACE_fieldDressing",8},{"ACE_morphine",2},{"ACE_epinephrine",1},{"ACE_tourniquet",1},{"ACE_splint",1}};
			basicMedVest[] = {};
			advMedUniform[] = {{"ACE_fieldDressing",8},{"ACE_morphine",2},{"ACE_epinephrine",1},{"ACE_tourniquet",1},{"ACE_splint",1}};
		
		}; 
	}; 

	class CUP_O_INS_Soldier_AK74 {
		displayName = "CUP_O_INS_Soldier_AK74";

		primaryWeapon[] = {"CUP_arifle_AKS74U","CUP_muzzle_mfsup_Flashhider_545x39_Black","","",{"CUP_30Rnd_545x39_AK74M_M",30},{},""};
		secondaryWeapon[] = {};
		handgunWeapon[] = {};

		uniformClass[] = {CHDKZ_UNIFORM}; 
		headgearClass[] = {CHDKZ_HEADGEAR}; 
		facewearClass[] = {CHDKZ_FACEWEAR};
		vestClass = "CUP_V_I_Carrier_Belt";

		linkedItems[] = {"ItemMap","","","ItemCompass","ItemWatch",""};

		uniformItems[] = {};
		vestItems[] = {{"CUP_30Rnd_545x39_AK74M_M",8,30},{"MiniGrenade",2,1}};

		basicMedUniform[] = {{"ACE_fieldDressing",8},{"ACE_morphine",2},{"ACE_epinephrine",1},{"ACE_tourniquet",1},{"ACE_splint",1}};
		basicMedVest[] = {};
		advMedUniform[] = {{"ACE_fieldDressing",8},{"ACE_morphine",2},{"ACE_epinephrine",1},{"ACE_tourniquet",1},{"ACE_splint",1}};

	};

	class CUP_O_INS_Soldier_GL {
		// Requires the following DLC:
		// Contact Platform
		displayName = "CUP_O_INS_Soldier_GL";

		primaryWeapon[] = {"sgun_HunterShotgun_01_F","","","",{"2Rnd_12Gauge_Pellets",2},{},""};
		secondaryWeapon[] = {};
		handgunWeapon[] = {};

		uniformClass[] = {CHDKZ_UNIFORM}; 
		headgearClass[] = {CHDKZ_HEADGEAR}; 
		facewearClass[] = {CHDKZ_FACEWEAR};
		vestClass = "CUP_V_I_Carrier_Belt";

		linkedItems[] = {"ItemMap","","","ItemCompass","ItemWatch",""};

		uniformItems[] = {};
		vestItems[] = {{"MiniGrenade",2,1},{"SmokeShell",2,1},{"ACE_2Rnd_12Gauge_Pellets_No0_Buck",15,2}};

		basicMedUniform[] = {{"ACE_fieldDressing",8},{"ACE_morphine",2},{"ACE_epinephrine",1},{"ACE_tourniquet",1},{"ACE_splint",1}};
		basicMedVest[] = {};
		advMedUniform[] = {{"ACE_fieldDressing",8},{"ACE_morphine",2},{"ACE_epinephrine",1},{"ACE_tourniquet",1},{"ACE_splint",1}};

	};

	class CUP_O_INS_Soldier {
		displayName = "CUP_O_INS_Soldier";

		primaryWeapon[] = {"CUP_arifle_AKS","CUP_muzzle_mfsup_Flashhider_762x39_Black","CUP_acc_Flashlight","",{"CUP_30Rnd_762x39_AK47_M",30},{},""};
		secondaryWeapon[] = {};
		handgunWeapon[] = {};

		uniformClass[] = {CHDKZ_UNIFORM}; 
		headgearClass[] = {CHDKZ_HEADGEAR}; 
		facewearClass[] = {CHDKZ_FACEWEAR};
		vestClass = "CUP_V_I_Carrier_Belt";

		linkedItems[] = {"ItemMap","","","ItemCompass","ItemWatch",""};

		uniformItems[] = {};
		vestItems[] = {{"MiniGrenade",2,1},{"CUP_30Rnd_762x39_AK47_M",8,30}};

		basicMedUniform[] = {{"ACE_fieldDressing",8},{"ACE_morphine",2},{"ACE_epinephrine",1},{"ACE_tourniquet",1},{"ACE_splint",1}};
		basicMedVest[] = {};
		advMedUniform[] = {{"ACE_fieldDressing",8},{"ACE_morphine",2},{"ACE_epinephrine",1},{"ACE_tourniquet",1},{"ACE_splint",1}};
	};

	class CUP_O_INS_Saboteur {
		displayName = "CUP_O_INS_Saboteur";

		primaryWeapon[] = {"CUP_arifle_AKS74U","CUP_muzzle_PBS4","","",{"CUP_30Rnd_545x39_AK74M_M",30},{},""};
		secondaryWeapon[] = {};
		handgunWeapon[] = {};

		uniformClass[] = {CHDKZ_UNIFORM}; 
		headgearClass[] = {CHDKZ_HEADGEAR}; 
		facewearClass[] = {CHDKZ_FACEWEAR};
		vestClass = "CUP_V_RUS_Smersh_New_Light";

		linkedItems[] = {"ItemMap","","","ItemCompass","ItemWatch",""};

		uniformItems[] = {};
		vestItems[] = {{"CUP_30Rnd_545x39_AK74M_M",8,30},{"MiniGrenade",4,1},{"ACE_M14",4,1},{"ACE_M84",2,1},{"SmokeShell",4,1}};

		basicMedUniform[] = {{"ACE_fieldDressing",8},{"ACE_morphine",2},{"ACE_epinephrine",1},{"ACE_tourniquet",1},{"ACE_splint",1}};
		basicMedVest[] = {};
		advMedUniform[] = {{"ACE_fieldDressing",8},{"ACE_morphine",2},{"ACE_epinephrine",1},{"ACE_tourniquet",1},{"ACE_splint",1}};
		
	};

	class CUP_O_INS_Sniper {
		displayName = "CUP_O_INS_Sniper";

		primaryWeapon[] = {"CUP_srifle_SVD","","","CUP_optic_PSO_1",{"ACE_10Rnd_762x54_Tracer_mag",10},{},""};
		secondaryWeapon[] = {};
		handgunWeapon[] = {};

		uniformClass[] = {CHDKZ_UNIFORM}; 
		headgearClass[] = {CHDKZ_HEADGEAR}; 
		facewearClass[] = {CHDKZ_FACEWEAR};
		vestClass = "CUP_V_I_Carrier_Belt";

		linkedItems[] = {"ItemMap","","","ItemCompass","ItemWatch",""};

		uniformItems[] = {};
		vestItems[] = {{"ACE_10Rnd_762x54_Tracer_mag",10,10}};

		basicMedUniform[] = {{"ACE_fieldDressing",8},{"ACE_morphine",2},{"ACE_epinephrine",1},{"ACE_tourniquet",1},{"ACE_splint",1}};
		basicMedVest[] = {};
		advMedUniform[] = {{"ACE_fieldDressing",8},{"ACE_morphine",2},{"ACE_epinephrine",1},{"ACE_tourniquet",1},{"ACE_splint",1}};

	};

	//Slightly modified Armed Civilians classes from CHDKZ also
	class CUP_O_INS_Woodlander1 {
		displayName = "CUP_O_INS_Woodlander1";

		primaryWeapon[] = {"CUP_arifle_AKM","","","",{"CUP_30Rnd_762x39_AK47_M",30},{},""};
		secondaryWeapon[] = {};
		handgunWeapon[] = {};

		uniformClass = "CUP_U_O_Woodlander_01";
		headgearClass = "CUP_H_C_Ushanka_01";
		facewearClass = "CUP_G_Bandanna_blk";

		linkedItems[] = {"ItemMap","","","ItemCompass","ItemWatch",""};

		uniformItems[] = {{"CUP_30Rnd_762x39_AK47_M",6,30}};

		basicMedUniform[] = {{"ACE_fieldDressing",8},{"ACE_morphine",2},{"ACE_epinephrine",1}};
		advMedUniform[] = {{"ACE_fieldDressing",8},{"ACE_morphine",2},{"ACE_epinephrine",1}};

	};

	class CUP_O_INS_Villager4 {
		displayName = "CUP_O_INS_Villager4";

		primaryWeapon[] = {"CUP_srifle_CZ550","","","",{"CUP_5x_22_LR_17_HMR_M",5},{},""};
		secondaryWeapon[] = {};
		handgunWeapon[] = {};

		uniformClass = "CUP_U_O_Villager_04";
		headgearClass = "CUP_H_C_Beret_04";
		facewearClass = "G_Bandanna_blk";

		linkedItems[] = {"ItemMap","","","ItemCompass","ItemWatch",""};

		uniformItems[] = {{"CUP_5x_22_LR_17_HMR_M",9,5}};

		basicMedUniform[] = {{"ACE_fieldDressing",8},{"ACE_epinephrine",1},{"ACE_morphine",2}};
		advMedUniform[] = {{"ACE_fieldDressing",8},{"ACE_morphine",2},{"ACE_epinephrine",1}};
		
	};

	class CUP_O_INS_Worker2 {
		displayName = "CUP_O_INS_Worker2";

		primaryWeapon[] = {"CUP_arifle_AKS","","","",{"CUP_30Rnd_762x39_AK47_M",30},{},""};
		secondaryWeapon[] = {};
		handgunWeapon[] = {};

		uniformClass = "CUP_U_O_Worker_02";
		headgearClass = "CUP_H_C_Beanie_02";
		facewearClass = "G_Bandanna_khk";

		linkedItems[] = {"ItemMap","","","ItemCompass","ItemWatch",""};

		uniformItems[] = {{"CUP_30Rnd_762x39_AK47_M",6,30}};

		basicMedUniform[] = {{"ACE_fieldDressing",8},{"ACE_morphine",2},{"ACE_epinephrine",1}};
		advMedUniform[] = {{"ACE_fieldDressing",8},{"ACE_morphine",2},{"ACE_epinephrine",1}};

	};

	class CUP_O_INS_Woodlander2 {
		displayName = "CUP_O_INS_Woodlander2";

		primaryWeapon[] = {"CUP_srifle_CZ550","","","",{"CUP_5x_22_LR_17_HMR_M",5},{},""};
		secondaryWeapon[] = {};
		handgunWeapon[] = {};

		uniformClass = "CUP_U_O_Woodlander_02";
		headgearClass = "CUP_H_C_Ushanka_02";

		linkedItems[] = {"ItemMap","","","ItemCompass","ItemWatch",""};

		uniformItems[] = {{"CUP_5x_22_LR_17_HMR_M",8,5}};

		basicMedUniform[] = {{"ACE_fieldDressing",8},{"ACE_morphine",2},{"ACE_epinephrine",1}};
		advMedUniform[] = {{"ACE_fieldDressing",8},{"ACE_morphine",2},{"ACE_epinephrine",1}};

	};

	class CUP_O_INS_Woodlander3 {
		displayName = "CUP_O_INS_Woodlander3";

		primaryWeapon[] = {"CUP_arifle_AKS","","","",{"CUP_30Rnd_762x39_AK47_M",30},{},""};
		secondaryWeapon[] = {};
		handgunWeapon[] = {};

		uniformClass = "CUP_U_O_Woodlander_03";
		headgearClass = "CUP_H_C_Ushanka_03";
		facewearClass = "CUP_G_Bandanna_khk";

		linkedItems[] = {"ItemMap","","","ItemCompass","ItemWatch",""};

		uniformItems[] = {{"CUP_30Rnd_762x39_AK47_M",6,30}};

		basicMedUniform[] = {{"ACE_fieldDressing",8},{"ACE_morphine",2},{"ACE_epinephrine",1}};
		advMedUniform[] = {{"ACE_fieldDressing",8},{"ACE_morphine",2},{"ACE_epinephrine",1}};

	};

	class O_soldier_Melee {
		displayName = "O_soldier_Melee";

		primaryWeapon[] = {};
		secondaryWeapon[] = {};
		handgunWeapon[] = {"Pipe_aluminium","","","",{},{},""};

		uniformClass = "CUP_U_C_Woodlander_03";
		facewearClass = "G_Balaclava_blk";

		linkedItems[] = {"ItemMap","","","ItemCompass","ItemWatch",""};

		uniformItems[] = {};

		basicMedUniform[] = {{"ACE_fieldDressing",4},{"ACE_morphine",1},{"ACE_epinephrine",1},{"ACE_tourniquet",1}};
		advMedUniform[] = {{"ACE_fieldDressing",4},{"ACE_morphine",1},{"ACE_epinephrine",1},{"ACE_tourniquet",1}};

	};


	
	//Classes inherting from other predefined classes above
	class CUP_O_INS_Soldier_AA: CUP_O_INS_Soldier_AK74 {}; 
	class CUP_O_INS_Pilot: CUP_O_INS_Soldier_AK74 {}; 
	class CUP_O_INS_Medic: CUP_O_INS_Soldier_AK74 {};
	class CUP_O_INS_Story_Lopotev: CUP_O_INS_Soldier_AK74 {}; 
	class CUP_O_INS_Story_Bardak: CUP_O_INS_Soldier_AK74 {}; 
	class CUP_O_INS_Soldier_Engineer: CUP_O_INS_Soldier_AK74 {};
	class CUP_O_INS_Soldier_Ammo: CUP_O_INS_Soldier_AK74 {};
	class CUP_O_INS_Crew: CUP_O_INS_Soldier_AK74 {};
	class CUP_O_INS_Soldier_Exp: CUP_O_INS_Soldier_AK74 {}; 
	class CUP_O_INS_Officer: CUP_O_INS_Soldier_AK74 {};
	class CUP_O_INS_Soldier_LAT: CUP_O_INS_Soldier_AK74 
	{
		secondaryWeapon[] = {"CUP_launch_RPG18_Loaded","","","",{"CUP_RPG18_M",1},{},""};
	};

	//Next for the civilian class to be spawned by Zeus 
	class C_man_1 {
		displayName = "C_man_1";

		primaryWeapon[] = {};
		secondaryWeapon[] = {};
		handgunWeapon[] = {};

		uniformClass[] = {CIV_UNIFORM}; 
		headgearClass[] = {CIV_HEADGEAR}; 

		linkedItems[] = {"ItemMap","","","ItemCompass","ItemWatch",""};

		uniformItems[] = {};

		basicMedUniform[] = {};
	};
 
};