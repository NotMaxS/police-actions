// CfgDebriefingSections.hpp
// Defines custom "debriefing sections". These can be used to display additional information at the end of a mission.
// Has the ACEX Kill Tracker enabled by default.

class SLV_OperationSummary {
	title = "Operation Summary"; 
	variable = "SLV_mission_endSummary";
}; 

class acex_killTracker {
	title = "ACEX Killed Events";
	variable = "acex_killTracker_outputText";
};