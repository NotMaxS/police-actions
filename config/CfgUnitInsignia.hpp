// Unit Insignias
// Usually visible on the sleeve of a unit
// https://community.bistudio.com/wiki/Arma_3_Unit_Insignia

/*
class example_insignia
{
	displayName = "Example";
	author = "Author";
	texture = "insignia.paa";
}
*/

class tmtm_cpd
{
	displayName = "Chernarus Police";
	author = "StellaStrela";
	texture = "media\patches\cpd_ca.paa";
	material = "\A3\Ui_f\data\GUI\Cfg\UnitInsignia\default_insignia.rvmat";
};
