// XPTItemCargos.hpp
// Used for defining ammo box and vehicle item cargos
// XPTVehicleLoadouts can pull definitions from here in order to supply vehicles
// Supports sub-class randomization, if a definition has multiple sub-classes, the script will automatically select one of them to apply to the vehicle/box.


class itemCargos
{

	class myBTR 
	{
		items[] = 
		{
			//Weapons/tools
			{"CUP_30Rnd_545x39_AK_M", 40}, 
			{"CUP_8Rnd_762x25_TT",20}, 
			{"SmokeShell", 15}, 
			{"HandGrenade", 30}, 
			{"ACE_M84", 20}, 	
			{"ACE_CableTie",10}, 
			{"CUP_1Rnd_HE_GP25_M", 30}, 
			{"150Rnd_762x54_Box", 15}, 
			{"CUP_1Rnd_SMOKE_GP25_M", 15}, 
			{"CUP_OG7_M", 10},
			{"CUP_TBG7V_M", 8}, 
			{"ToolKit",1}, 
			{"B_FieldPack_green_F", 1}, 
			{"CUP_launch_RShG2", 6}, 
			{"ACE_Chemlight_HiBlue", 15},
			{"ACE_Chemlight_HiGreen", 15},
			{"ACE_Chemlight_HiRed",15},
			{"ACE_Chemlight_HiWhite",15},
			{"ACE_Chemlight_HiYellow",15},
			{"ACE_HandFlare_Green",8},
			{"ACE_HandFlare_Red",8},
			{"ACE_HandFlare_White",8},
			{"ACE_HandFlare_Yellow",8}, 
			//Medical items 
			{"ACE_quikclot",20},
			{"ACE_epinephrine",8},
			{"ACE_morphine",15},
			{"ACE_tourniquet",10}, 
			{"ACE_splint",10}, 
			//Specialized gear in the form of spray cans and wire cutter
			{"ACE_SpraypaintBlue",4},
			{"ACE_SpraypaintGreen",4},
			{"ACE_SpraypaintRed",2},
			{"ACE_SpraypaintYellow",4}, 
			{"ACE_wirecutter",2}


		};
	};


	class mySquadCar 
	{
		items[] = 
		{
			//Weapons/tools
			{"CUP_30Rnd_545x39_AK_M", 20}, 
			{"CUP_8Rnd_762x25_TT",10}, 
			{"SmokeShell", 8}, 
			{"HandGrenade", 15}, 
			{"ACE_M84", 15}, 	
			{"ACE_CableTie",10}, 
			{"CUP_1Rnd_HE_GP25_M", 10}, 
			{"150Rnd_762x54_Box", 5}, 
			{"CUP_1Rnd_SMOKE_GP25_M", 15}, 
			{"CUP_OG7_M", 4},
			{"CUP_TBG7V_M", 4}, 
			{"ToolKit",1}, 
			{"B_FieldPack_green_F", 1}, 
			{"CUP_launch_RShG2", 4}, 
			{"ACE_Chemlight_HiBlue", 8},
			{"ACE_Chemlight_HiGreen", 8},
			{"ACE_Chemlight_HiRed",8},
			{"ACE_Chemlight_HiWhite",8},
			{"ACE_Chemlight_HiYellow",8},
			{"ACE_HandFlare_Green",8},
			{"ACE_HandFlare_Red",4},
			{"ACE_HandFlare_White",4},
			{"ACE_HandFlare_Yellow",4}, 
			//Medical items 
			{"ACE_quikclot",20},
			{"ACE_epinephrine",8},
			{"ACE_morphine",15},
			{"ACE_tourniquet",10}, 
			{"ACE_splint",10}, 
			//Specialized gear in the form of spray cans and wirecutters
			{"ACE_SpraypaintBlue",2},
			{"ACE_SpraypaintGreen",2},
			{"ACE_SpraypaintRed",2},
			{"ACE_SpraypaintYellow",2}, 
			{"ACE_wirecutter",1}

		};
	};

	class myAmbulance 
	{
		items[] = 
		{
			//Weapons/tools
			{"CUP_8Rnd_762x25_TT",15}, 
			{"SmokeShell", 15}, 
			{"B_FieldPack_green_F", 1}, 
			{"ACE_Chemlight_HiBlue", 15},
			{"ACE_Chemlight_HiGreen", 15},
			{"ACE_Chemlight_HiRed",15},
			{"ACE_Chemlight_HiWhite",15},
			{"ACE_Chemlight_HiYellow",15},
			{"ACE_HandFlare_Green",15},
			{"ACE_HandFlare_Red",15},
			{"ACE_HandFlare_White",15},
			{"ACE_HandFlare_Yellow",15}, 
			//Medical items 
			{"ACE_epinephrine",20},
			{"ACE_morphine",30},
			{"ACE_tourniquet",20}, 
			{"ACE_splint",20},
			{"ACE_personalAidKit",2},
			{"ACE_surgicalKit",2},
			{"ACE_quikclot",30},
			{"ACE_packingBandage",30},
			{"ACE_elasticBandage",30},
			{"ACE_bloodIV",15},
			{"ACE_bloodIV_250",15},
			{"ACE_bloodIV_500",15}

		};
	};

	class myUAZ 
	{
		items[] = 
		{
			//Weapons/tools
			{"CUP_30Rnd_545x39_AK_M", 10}, 
			{"CUP_8Rnd_762x25_TT",5}, 
			{"SmokeShell", 8}, 
			{"HandGrenade", 8}, 
			{"ToolKit",1}, 
			{"B_FieldPack_green_F", 1}, 
			{"CUP_launch_RShG2", 2}, 
			{"ACE_Tripod",1},
			{"10Rnd_127x54_Mag",15}, 
			{"ACE_Chemlight_HiBlue", 15},
			{"ACE_Chemlight_HiGreen", 15},
			{"ACE_Chemlight_HiRed",15},
			{"ACE_Chemlight_HiWhite",15},
			{"ACE_Chemlight_HiYellow",15},
			{"ACE_HandFlare_Green",15},
			{"ACE_HandFlare_Red",15},
			{"ACE_HandFlare_White",15},
			{"ACE_HandFlare_Yellow",15}, 
			//Medical items 
			{"ACE_quikclot",20},
			{"ACE_epinephrine",8},
			{"ACE_morphine",15},
			{"ACE_tourniquet",5}, 
			{"ACE_splint",5},
			//Specialized gear in the form of spray cans and wirecutters
			{"ACE_SpraypaintBlue",2},
			{"ACE_SpraypaintGreen",2},
			{"ACE_SpraypaintRed",2},
			{"ACE_SpraypaintYellow",2}, 
			{"ACE_wirecutter",1}
		};
	};

	class myCommandCar
	{
		items[] = 
		{
			{"CUP_30Rnd_545x39_AK_M", 10}, 
			{"CUP_8Rnd_762x25_TT",5}, 
			{"SmokeShell", 8}, 
			{"HandGrenade", 8}, 
			{"ToolKit",1}, 
			{"B_FieldPack_green_F", 1}, 
			{"ACE_Chemlight_HiBlue", 15},
			{"ACE_Chemlight_HiGreen", 15},
			{"ACE_Chemlight_HiRed",15},
			{"ACE_Chemlight_HiWhite",15},
			{"ACE_Chemlight_HiYellow",15},
			{"ACE_HandFlare_Green",15},
			{"ACE_HandFlare_Red",15},
			{"ACE_HandFlare_White",15},
			{"ACE_HandFlare_Yellow",15}, 
			//Medical items 
			{"ACE_quikclot",20},
			{"ACE_epinephrine",8},
			{"ACE_morphine",15},
			{"ACE_tourniquet",5}, 
			{"ACE_splint",5},
			//Specialized gear in the form of spray cans and wirecutters
			{"ACE_SpraypaintBlue",2},
			{"ACE_SpraypaintGreen",2},
			{"ACE_SpraypaintRed",2},
			{"ACE_SpraypaintYellow",2}, 
			{"ACE_wirecutter",1}
		}; 
	};

	class myJouranlistCar
	{
		items[] = 
		{
			{"ToolKit",2}, 
			{"B_CivilianBackpack_01_Everyday_Black_F", 2}, 
			//Medical items 
			{"ACE_quikclot",20},
			{"ACE_epinephrine",8},
			{"ACE_morphine",15},
			{"ACE_tourniquet",5}, 
			{"ACE_splint",5},
		}; 
	};

};