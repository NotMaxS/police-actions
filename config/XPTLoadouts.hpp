// XPTloadouts.hpp
// Used for defining advanced respawn loadouts for players
// Default behaviour is to check if the player unit has a special loadout defined. Otherwise, it will check to see if the classname matches a loadout
// Advanced functionality allows mission creator to define exactly where items are placed in unit inventories
// Also supports sub-loadout randomization. If a loadout has sub-classes defined, the script will automatically select one of them to apply to the unit.
class loadouts
{	
	//Randomized loadout defines 
	#define SLV_UNIFORM "tmtm_u_bdu_kneepad_black", "tmtm_u_bdu_dubokb", "tmtm_u_bdu_kneepad_dubokb", "tmtm_u_bdu_kneepad_roll_dubokb", "tmtm_u_bdu_roll_highf_dubokb_mix", "tmtm_u_bdu_kneepad_dubokgb", "tmtm_u_bdu_kneepad_roll_dubokgb", "tmtm_u_bdu_tshirt_dubokgb", "tmtm_u_bdu_kneepad_dubokpb", "tmtm_u_bdu_dubokt", "tmtm_u_bdu_kneepad_dubokt", "tmtm_u_bdu_kneepad_roll_dubokt", "tmtm_u_bdu_roll_highf_dubokt_mix"
	#define SLV_HEADGEAR "tmtm_h_k6_cpd_black", "tmtm_h_k6_cpd_black_visorDown", "tmtm_h_k6_cpd_black_visorUp", "tmtm_h_k6_cpd_dubokb", "tmtm_h_k6_cpd_dubokb_visorDown", "tmtm_h_k6_cpd_dubokb_visorUp", "tmtm_h_k6_cpd_dubokgb", "tmtm_h_k6_cpd_dubokgb_visorDown", "tmtm_h_k6_cpd_dubokgb_visorUp", "tmtm_h_k6_cpd_olive", "tmtm_h_k6_cpd_olive_visorDown", "tmtm_h_k6_cpd_olive_visorUp"
	#define SLV_FACEWEAR "CUP_G_RUS_Ratnik_Balaclava_Olive_1", "CUP_G_RUS_Ratnik_Balaclava_Winter_Green_2", "tmtm_f_balaclava_slit_black", "tmtm_f_balaclava_slit_loose_black", "tmtm_f_balaclava_slit_ameba", "tmtm_f_balaclava_slit_loose_ameba", "tmtm_f_balaclava_slit_dubokb", "tmtm_f_balaclava_slit_loose_dubokb", "tmtm_f_balaclava_slit_dubokgb", "tmtm_f_balaclava_slit_loose_dubokgb", "tmtm_f_balaclava_slit_olive", "tmtm_f_balaclava_slit_loose_olive"
	#define SLV_VEST "tmtm_v_pasgt_cpd_ameba", "tmtm_v_pasgt_cpd_black_alt", "tmtm_v_pasgt_cpd_dubokgb", "tmtm_v_pasgt_cpd_olive_alt" 
	#define SLV_BACKPACK "tmtm_b_rsSmersh_ak_olive", "tmtm_b_rsSmersh_ak_brown"
	
	//Defines for specialized roles 
	#define SLV_UNIFORM_MEDIC "U_lxWS_ION_Casual3", "U_lxWS_ION_Casual5", "U_C_IDAP_Man_cargo_F", "U_C_Journalist", "U_Marshal"
	#define SLV_UNIFORM_CREW "tmtm_u_bdu_black", "tmtm_u_bdu_tshirt_black", "tmtm_u_bdu_tshirt_dubokgb", "tmtm_u_bdu_dubokgb_mix_b", "U_I_C_Soldier_Para_4_F"

	#define SLV_HEADGEAR_CREW "H_Tank_eaf_F", "lxWS_H_Tank_tan_F", "CUP_H_SLA_TankerHelmet", "CUP_H_TK_TankerHelmet"
	#define SLV_HEADGEAR_MEDIC "lxWS_H_ssh40_white", "CUP_H_PMC_Cap_Back_Burberry", "H_Bandanna_khk", ""
	#define SLV_HEADGEAR_MARKSMAN "CUP_H_PMC_Cap_Grey", "CUP_H_PMC_Beanie_Black", "CUP_H_PMC_Beanie_Khaki", "CUP_H_PMC_Cap_Back_Grey"

	#define SLV_FACEWEAR_MEDIC "G_Respirator_white_F", "G_Bandanna_tan", "G_Bandanna_blk", ""
	#define SLV_FACEWEAR_MARKSMAN "tmtm_f_balaclava_slit_black", "tmtm_f_balaclava_slit_loose_black", "tmtm_f_balaclava_slit_gray", "tmtm_f_balaclava_slit_loose_gray"
	
	#define SLV_VEST_AT "tmtm_v_pasgt_cpd_ameba_ep", "tmtm_v_pasgt_cpd_black_ep", "tmtm_v_pasgt_cpd_dubokgb_ep", "tmtm_v_pasgt_cpd_olive_ep"

	#define SLV_BACKPACK_GREN "tmtm_b_rsSmersh_vogSPP_brown", "tmtm_b_rsSmersh_vogSPP_olive"
	#define SLV_BACKPACK_MG "tmtm_b_rsSmersh_pkpSPP_olive", "tmtm_b_rsSmersh_pkpSPP_brown"
	#define SLV_BACKPACK_AT "CUP_B_RPGPack_Khaki"
	#define SLV_BACKPACK_CREW "B_FieldPack_green_F", "B_FieldPack_taiga_F", "B_FieldPack_oucamo"
	#define SLV_BACKPACK_MEDIC "CUP_B_HikingPack_Civ", "CUP_C_PHOENIX_FIRSTAID"
	#define SLV_BACKPACK_MARKSMAN "tmtm_b_rsSmersh_svd_brown", "tmtm_b_rsSmersh_svd_olive"

	//Standardized medical and linked item defines 
	#define ADV_MEDICAL {"ACE_quikclot",8},{"ACE_epinephrine",2},{"ACE_morphine",2},{"ACE_tourniquet",4}, {"ACE_splint",1}
	#define LINKED_ITEM_LDR "ItemMap","","TFAR_fadak","ItemCompass","ItemWatch","tmtm_nvg_wool_gloves_cut_olive"
	#define LINKED_ITEM_STD "ItemMap","","TFAR_fadak","ItemCompass","ItemWatch","tmtm_nvg_wool_gloves_cut_black"

	//Primary rifle for general infantry 
	#define PRIMARY_GENERAL_AKSU {"CUP_arifle_AKS74U_railed","CUP_muzzle_mfsup_Flashhider_545x39_Black","CUP_acc_Flashlight","",{"CUP_30Rnd_545x39_AK_M",30},{},""}
	#define PRIMARY_GENERAL_AKS {"CUP_arifle_AKS74_Early","CUP_muzzle_mfsup_Flashhider_545x39_Black","CUP_acc_Flashlight","",{"CUP_30Rnd_545x39_AK_M",30},{},""}
	#define PRIMARY_GENERAL_AK 	{"CUP_arifle_AK74_Early","CUP_muzzle_mfsup_Flashhider_545x39_Black","CUP_acc_Flashlight","",{"CUP_30Rnd_545x39_AK_M",30},{},""}

	//Primary rifle for those using gp 25
	#define PRIMARY_GREN_AK {"CUP_arifle_AK74_GL_Early","CUP_muzzle_mfsup_Flashhider_545x39_Black","CUP_acc_Flashlight","",{"CUP_30Rnd_545x39_AK_M",30},{"CUP_1Rnd_HE_GP25_M",1},""}
	#define PRIMARY_GREN_AKS {"CUP_arifle_AKS74_GL_Early","CUP_muzzle_mfsup_Flashhider_545x39_Black","CUP_acc_Flashlight","",{"CUP_30Rnd_545x39_AK_M",30},{"CUP_1Rnd_HE_GP25_M",1},""}


	class B_Soldier_SL_F {
		class AKSU {
			displayName = "squad leader";

			primaryWeapon[] = PRIMARY_GENERAL_AKSU;
			secondaryWeapon[] = {};
			handgunWeapon[] = {"CUP_hgun_TT","","","",{"CUP_8Rnd_762x25_TT",8},{},""};
			binocular = "Binocular";


			uniformClass[] = {SLV_UNIFORM}; 
			headgearClass[] = {SLV_HEADGEAR}; 
			facewearClass[] = {SLV_FACEWEAR}; 
			vestClass[] = {SLV_VEST_AT}; 
			backpackClass[] = {SLV_BACKPACK}; 

			linkedItems[] = {LINKED_ITEM_LDR}; 

			uniformItems[] = {{"CUP_8Rnd_762x25_TT",2, 8}};
			vestItems[] = {{"HandGrenade",4,1} ,{"CUP_30Rnd_545x39_AK_M",8,30}};
			backpackItems[] = {{"ACE_CableTie",4}, {"ACE_M84",2,1},{"SmokeShell",4,1}, {"ACE_DAGR", 1}, {"ACE_SpraypaintRed",1}};

			basicMedUniform[] = {ADV_MEDICAL};
			basicMedVest[] = {};
			basicMedBackpack[] = {};

			advMedUniform[] = {ADV_MEDICAL};
			};
			class AKS: AKSU 
			{
				primaryWeapon[] = PRIMARY_GENERAL_AKS;

			}; 
			class AK: AKSU 
			{
				primaryWeapon[] = PRIMARY_GENERAL_AK;
			}; 

	};

	class B_Soldier_TL_F {
		class AKSGREN
		{
			displayName = "team lead";

			primaryWeapon[] = PRIMARY_GREN_AKS; 
			secondaryWeapon[] = {};
			handgunWeapon[] = {"CUP_hgun_TT","","","",{"CUP_8Rnd_762x25_TT",8},{},""};

			//uniformClass[] = {SLV_UNIFORM};
			uniformClass[] = {SLV_UNIFORM}; 
			headgearClass[] = {SLV_HEADGEAR}; 
			facewearClass[] = {SLV_FACEWEAR}; 
			vestClass[] = {SLV_VEST_AT}; 
			backpackClass[] = {SLV_BACKPACK_GREN}; 

			linkedItems[] = {LINKED_ITEM_LDR};

			uniformItems[] = {{"CUP_8Rnd_762x25_TT",2, 8}};
			vestItems[] = {{"HandGrenade",4,1} ,{"CUP_30Rnd_545x39_AK_M",8,30}};
			backpackItems[] = {{"ACE_CableTie",4}, {"ACE_M84",2,1},{"SmokeShell",8,1}, {"CUP_1Rnd_HE_GP25_M",8,1}, {"ACE_DAGR", 1}, {"ACE_SpraypaintRed",1}};

			basicMedUniform[] = {ADV_MEDICAL};
			basicMedVest[] = {};
			basicMedBackpack[] = {};

			advMedUniform[] = {ADV_MEDICAL};
		};

		class AKGREN: AKSGREN 
		{
			primaryWeapon[] = PRIMARY_GREN_AK; 
		};  
	};

	class B_soldier_AR_F {
		displayName = "machine gunner";

		primaryWeapon[] = {"CUP_lmg_PKM_B50_vfg","CUP_muzzle_mfsup_Flashhider_PK_Black","CUP_acc_Flashlight","",{"150Rnd_762x54_Box",150},{},""};
		secondaryWeapon[] = {};
		handgunWeapon[] = {"CUP_hgun_TT","","","",{"CUP_8Rnd_762x25_TT",8},{},""};

		//uniformClass[] = {SLV_UNIFORM};
		uniformClass[] = {SLV_UNIFORM}; 
		headgearClass[] = {SLV_HEADGEAR}; 
		facewearClass[] = {SLV_FACEWEAR}; 
		vestClass[] = {SLV_VEST_AT}; 
		backpackClass[] = {SLV_BACKPACK_MG}; 

		linkedItems[] = {LINKED_ITEM_STD};

		uniformItems[] = {{"CUP_8Rnd_762x25_TT",2, 8}};
		vestItems[] = {{"HandGrenade",2,1}, {"SmokeShell",2,1}, {"ACE_SpraypaintRed",1}, {"150Rnd_762x54_Box",2,150}};
		backpackItems[] = {{"150Rnd_762x54_Box",2,150}, {"ACE_DAGR", 1}, {"ACE_CableTie",4}};

			basicMedUniform[] = {ADV_MEDICAL};
			basicMedVest[] = {};
			basicMedBackpack[] = {};

			advMedUniform[] = {ADV_MEDICAL};
	};

	class B_Soldier_GL_F {
		class AKGREN 
		{
			displayName = "grenadier";

			primaryWeapon[] = PRIMARY_GREN_AK; 
			secondaryWeapon[] = {};
			handgunWeapon[] = {"CUP_hgun_TT","","","",{"CUP_8Rnd_762x25_TT",8},{},""};

			uniformClass[] = {SLV_UNIFORM}; 
			headgearClass[] = {SLV_HEADGEAR}; 
			facewearClass[] = {SLV_FACEWEAR}; 
			vestClass[] = {SLV_VEST_AT}; 
			backpackClass[] = {SLV_BACKPACK_GREN}; 

			linkedItems[] = {LINKED_ITEM_STD};

			uniformItems[] = {{"CUP_8Rnd_762x25_TT",2, 8}};
			vestItems[] = {{"HandGrenade",2,1}, {"ACE_SpraypaintRed",1}, {"CUP_30Rnd_545x39_AK_M",8,30}};
			backpackItems[] = {{"ACE_CableTie",4},{"CUP_1Rnd_HE_GP25_M",15,1},{"CUP_1Rnd_SMOKE_GP25_M",8,1},{"ACE_M84",4,1},{"SmokeShell",2,1}, {"ACE_DAGR", 1}};

			basicMedUniform[] = {ADV_MEDICAL};
			basicMedVest[] = {};
			basicMedBackpack[] = {};

			advMedUniform[] = {ADV_MEDICAL};
		}; 

		class AKSGREN: AKGREN 
		{
			primaryWeapon[] = PRIMARY_GREN_AKS; 
		}; 
	};

	class B_soldier_LAT_F {
		class AKSU
		{
			displayName = "rifleman LAT";

			primaryWeapon[] = PRIMARY_GENERAL_AKSU;		
			secondaryWeapon[] = {"CUP_launch_RPG7V","","","CUP_optic_PGO7V3",{"CUP_OG7_M",1},{},""};
			handgunWeapon[] = {"CUP_hgun_TT","","","",{"CUP_8Rnd_762x25_TT",8},{},""};

			uniformClass[] = {SLV_UNIFORM}; 
			headgearClass[] = {SLV_HEADGEAR}; 
			facewearClass[] = {SLV_FACEWEAR}; 
			vestClass[] = {SLV_VEST_AT}; 
			backpackClass[] = {SLV_BACKPACK_AT}; 

			linkedItems[] = {LINKED_ITEM_STD};

			uniformItems[] = {{"CUP_8Rnd_762x25_TT",2, 8}};
			vestItems[] = {{"ACE_CableTie",4},{"CUP_30Rnd_545x39_AK_M",8,30},{"HandGrenade",2,1},{"SmokeShell",2,1},{"ACE_M84",4,1}, {"ACE_DAGR", 1}, {"ACE_SpraypaintRed",1}};
			backpackItems[] = {{"CUP_OG7_M",2,1},{"CUP_TBG7V_M",1,1}};

			basicMedUniform[] = {ADV_MEDICAL};
			basicMedVest[] = {};
			basicMedBackpack[] = {};

			advMedUniform[] = {ADV_MEDICAL};
		}; 
		class AK: AKSU 
		{
			primaryWeapon[] = PRIMARY_GENERAL_AK; 
		}; 
		class AKS: AKSU 
		{
			primaryWeapon[] = PRIMARY_GENERAL_AKS; 
		}; 
	};

	class B_Soldier_F {
		class AKSU 
		{
			displayName = "breacher";

			primaryWeapon[] = PRIMARY_GENERAL_AKSU; 			
			secondaryWeapon[] = {"CUP_launch_RShG2_Loaded","","","",{"CUP_RSHG2_M",1},{},""};
			handgunWeapon[] = {"CUP_hgun_TT","","","",{"CUP_8Rnd_762x25_TT",8},{},""};

			uniformClass[] = {SLV_UNIFORM}; 
			headgearClass[] = {SLV_HEADGEAR}; 
			facewearClass[] = {SLV_FACEWEAR}; 
			vestClass[] = {SLV_VEST_AT}; 
			backpackClass[] = {SLV_BACKPACK_MG}; 

			linkedItems[] = {LINKED_ITEM_STD};

			uniformItems[] = {{"CUP_8Rnd_762x25_TT",2, 8}};
			vestItems[] = {{"HandGrenade",4,1} ,{"CUP_30Rnd_545x39_AK_M",10,30} };
			backpackItems[] = {{"ACE_CableTie",4},{"ACE_M84",4,1},{"SmokeShell",2,1},{"HandGrenade",2,1}, {"ACE_DAGR", 1}, {"ACE_SpraypaintRed",1}, {"ACE_Wirecutter", 1}};

			basicMedUniform[] = {ADV_MEDICAL};
			basicMedVest[] = {};
			basicMedBackpack[] = {};

			advMedUniform[] = {ADV_MEDICAL};
		};
		class AKS: AKSU 
		{
			primaryWeapon[] = PRIMARY_GENERAL_AKS; 
		}; 
		class AK: AKSU 
		{
			primaryWeapon[] = PRIMARY_GENERAL_AK; 
		}; 
	};

		class B_crew_F {

			displayName = "BTR crewman";
			
			primaryWeapon[] = PRIMARY_GENERAL_AKSU; 			
			secondaryWeapon[] = {};
			handgunWeapon[] = {};

			uniformClass[] = {SLV_UNIFORM_CREW}; 
			headgearClass[] = {SLV_HEADGEAR_CREW}; 
			facewearClass[] = {SLV_FACEWEAR}; 
			vestClass[] = {SLV_VEST}; 
			backpackClass[] = {SLV_BACKPACK_CREW}; 

			linkedItems[] = {LINKED_ITEM_LDR};

			uniformItems[] = {{"ACE_DAGR", 1}};
			vestItems[] = {{"CUP_30Rnd_545x39_AK_M",4,30}};
			backpackItems[] = {{"ToolKit",1},{"SmokeShell",4,1} , {"HandGrenade",2,1}, {"CUP_30Rnd_545x39_AK_M",2,30}};

			basicMedUniform[] = {ADV_MEDICAL};
			basicMedVest[] = {};
			basicMedBackpack[] = {};

			advMedUniform[] = {ADV_MEDICAL};
		};

		class B_medic_F {
		displayName = "medic";

		primaryWeapon[] = {};
		secondaryWeapon[] = {};
		handgunWeapon[] = {"CUP_hgun_TT","","","",{"CUP_8Rnd_762x25_TT",8},{},""};
		
		uniformClass[] ={SLV_UNIFORM_MEDIC};
		headgearClass[] = {SLV_HEADGEAR_MEDIC}; 
		facewearClass[] = {SLV_FACEWEAR_MEDIC}; 
		vestClass[] = {SLV_VEST_AT}; 
		backpackClass[] = {SLV_BACKPACK_MEDIC}; 

		linkedItems[] = {LINKED_ITEM_LDR};
	
		uniformItems[] = {};
		vestItems[] = {{"CUP_8Rnd_762x25_TT",8, 8}, {"ACE_DAGR", 1} , {"SmokeShell",4,1}};
		backpackItems[] = {};

		basicMedUniform[] = {ADV_MEDICAL};
		advMedUniform[] = {ADV_MEDICAL};
		basicMedVest[] = {};
		basicMedBackpack[] = {{"ACE_personalAidKit",1},{"ACE_surgicalKit",1},{"ACE_quikclot",25},{"ACE_packingBandage",15},{"ACE_elasticBandage",25},{"ACE_tourniquet",10},{"ACE_splint",10},{"ACE_morphine",15},{"ACE_epinephrine",15},{"ACE_bloodIV",8},{"ACE_bloodIV_250",8},{"ACE_bloodIV_500",8}};
		advMedBackpack[] = {{"ACE_personalAidKit",1},{"ACE_surgicalKit",1},{"ACE_quikclot",25},{"ACE_packingBandage",15},{"ACE_elasticBandage",25},{"ACE_tourniquet",10},{"ACE_splint",10},{"ACE_morphine",15},{"ACE_epinephrine",15},{"ACE_bloodIV",8},{"ACE_bloodIV_250",8},{"ACE_bloodIV_500",8}};

	};

	class B_Sharpshooter_F {
		// Requires the following DLC:
		// Marksmen
		// Apex
		displayName = "marksman";

		primaryWeapon[] = {"srifle_DMR_04_F","","CUP_acc_Flashlight","optic_KHS_blk",{"10Rnd_127x54_Mag",10},{},"bipod_02_F_blk"};
		secondaryWeapon[] = {};
		handgunWeapon[] = {"CUP_hgun_TT","","","",{"CUP_8Rnd_762x25_TT",8},{},""};

		uniformClass[] = {SLV_UNIFORM}; 
		headgearClass[] = {SLV_HEADGEAR_MARKSMAN};
		facewearClass[] = {SLV_FACEWEAR_MARKSMAN}; 
		vestClass[] = {SLV_VEST_AT};
		backpackClass[] = {SLV_BACKPACK_MARKSMAN}; 

		linkedItems[] = {LINKED_ITEM_LDR};

		uniformItems[] = {{"ACE_DAGR", 1}};
		vestItems[] = {{"ACE_RangeCard",1},{"10Rnd_127x54_Mag",10,10},{"SmokeShell",4,1}};
		backpackItems[] = {{"CUP_8Rnd_762x25_TT",8, 8}, {"ACE_CableTie",4}};

		basicMedUniform[] = {ADV_MEDICAL};
		basicMedVest[] = {};
		basicMedBackpack[] = {};

		advMedUniform[] = {ADV_MEDICAL};
	};

	class B_soldier_M_F {
		class AKSU
		{
			displayName = "spotter";

			primaryWeapon[] = PRIMARY_GENERAL_AKSU;		
			secondaryWeapon[] = {};
			handgunWeapon[] = {"CUP_hgun_TT","","","",{"CUP_8Rnd_762x25_TT",8},{},""};
			binocular = "Rangefinder";

			uniformClass[] = {SLV_UNIFORM}; 
			headgearClass[] = {SLV_HEADGEAR_MARKSMAN}; 
			facewearClass[] = {SLV_FACEWEAR_MARKSMAN}; 
			vestClass[] = {SLV_VEST_AT}; 
			backpackClass[] = {SLV_BACKPACK}; 

			linkedItems[] = {LINKED_ITEM_STD};

			uniformItems[] = {{"CUP_8Rnd_762x25_TT",2, 8}};
			vestItems[] = {{"ACE_CableTie",4},{"CUP_30Rnd_545x39_AK_M",8,30},{"HandGrenade",2,1},{"SmokeShell",2,1},{"ACE_M84",4,1}, {"ACE_DAGR", 1}};
			backpackItems[] = {{"ACE_Tripod",1}, {"ACE_wirecutter",1}, {"ACE_SpraypaintRed",1}};

			basicMedUniform[] = {ADV_MEDICAL};
			basicMedVest[] = {};
			basicMedBackpack[] = {};

			advMedUniform[] = {ADV_MEDICAL};
		}; 
		class AK: AKSU 
		{
			primaryWeapon[] = PRIMARY_GENERAL_AK; 
		}; 
		class AKS: AKSU 
		{
			primaryWeapon[] = PRIMARY_GENERAL_AKS; 
		}; 
	};

	class B_officer_F {
		class AKSU {
			displayName = "officer";

			primaryWeapon[] = PRIMARY_GENERAL_AKSU;
			secondaryWeapon[] = {};
			handgunWeapon[] = {"CUP_hgun_TT","","","",{"CUP_8Rnd_762x25_TT",8},{},""};
			binocular = "Binocular";


			uniformClass[] = {SLV_UNIFORM}; 
			headgearClass[] = {SLV_HEADGEAR_MARKSMAN}; 
			facewearClass[] = {SLV_FACEWEAR_MARKSMAN}; 
			vestClass[] = {SLV_VEST_AT}; 
			backpackClass = "TFAR_mr3000_rhs";

			linkedItems[] = {LINKED_ITEM_LDR}; 

			uniformItems[] = {{"CUP_8Rnd_762x25_TT",2, 8}};
			vestItems[] = {{"CUP_30Rnd_545x39_AK_M",8,30}, {"SmokeShell",4,1}};
			backpackItems[] = {{"ACE_DAGR", 1}, {"HandGrenade",4,1}, {"ACE_wirecutter",1}, {"ACE_SpraypaintRed",1}};

			basicMedUniform[] = {ADV_MEDICAL};
			basicMedVest[] = {};
			basicMedBackpack[] = {};

			advMedUniform[] = {ADV_MEDICAL};
			};
			class AKS: AKSU 
			{
				primaryWeapon[] = PRIMARY_GENERAL_AKS;

			}; 
			class AK: AKSU 
			{
				primaryWeapon[] = PRIMARY_GENERAL_AK;
			}; 

	};

	class B_Survivor_F {
		class AKSU {
			displayName = "command driver";

			primaryWeapon[] = PRIMARY_GENERAL_AKSU;
			secondaryWeapon[] = {};
			handgunWeapon[] = {"CUP_hgun_TT","","","",{"CUP_8Rnd_762x25_TT",8},{},""};

			uniformClass[] = {SLV_UNIFORM}; 
			headgearClass[] = {SLV_HEADGEAR_MARKSMAN}; 
			facewearClass[] = {SLV_FACEWEAR_MARKSMAN}; 
			vestClass[] = {SLV_VEST_AT}; 
			backpackClass = "TFAR_mr3000_rhs";

			linkedItems[] = {LINKED_ITEM_LDR}; 

			uniformItems[] = {{"CUP_8Rnd_762x25_TT",2, 8}};
			vestItems[] = {{"CUP_30Rnd_545x39_AK_M",8,30}, {"SmokeShell",4,1}};
			backpackItems[] = {{"ACE_DAGR", 1}, {"HandGrenade",4,1}, {"ACE_wirecutter",1}, {"ACE_SpraypaintRed",1}};

			basicMedUniform[] = {ADV_MEDICAL};
			basicMedVest[] = {};
			basicMedBackpack[] = {};

			advMedUniform[] = {ADV_MEDICAL};
			};
			class AKS: AKSU 
			{
				primaryWeapon[] = PRIMARY_GENERAL_AKS;

			}; 
			class AK: AKSU 
			{
				primaryWeapon[] = PRIMARY_GENERAL_AK;
			}; 

	};

	class C_man_1 {
		displayName = "C_man_1";

		primaryWeapon[] = {};
		secondaryWeapon[] = {};
		handgunWeapon[] = {"ACE_Flashlight_Maglite_ML300L","","","",{},{},""};
		binocular = "Camera_lxWS";

		uniformClass = "U_C_Journalist";
		headgearClass = "H_Cap_press";
		vestClass = "V_Press_F";

		linkedItems[] = {"ItemMap","","","ItemCompass","ItemWatch",""};

		uniformItems[] = {};
		vestItems[] = {{"ACE_DAGR", 1}};

		basicMedUniform[] = {ADV_MEDICAL};
		basicMedVest[] = {};
		advMedUniform[] = {ADV_MEDICAL};

	};


};