class AmbientAnimations
{
    version = 1.0;
    
    toggleAiFeatures[] = {
        "ANIM", 
        "AUTOTARGET", 
        "FSM", 
        "MOVE", 
        "TARGET",
        "lambs_danger_disableAI",
        "lambs_danger_dangerRadio"
    };

    class Animation
    {
        moves[] = {};
        hideWeapon = false;
    };

    // ********** Custom animations **********
    class STAND_1 : Animation
    {
        moves[] = {
            "HubStanding_idle1",
            "HubStanding_idle2",
            "HubStanding_idle3"
        };
    };
    class STAND_2 : Animation
    {
        moves[] = {
            "amovpercmstpslowwrfldnon",
            "amovpercmstpslowwrfldnon",
            "aidlpercmstpslowwrfldnon_g01",
            "aidlpercmstpslowwrfldnon_g02",
            "aidlpercmstpslowwrfldnon_g03",
            "aidlpercmstpslowwrfldnon_g05",
            "Acts_AidlPercMstpSloWWrflDnon_warmup_3_loop"
        };
    };
    class STAND_NO_WEAP_1 : Animation
    {
        moves[] = {
            "HubStandingUA_idle1",
            "HubStandingUA_idle2",
            "HubStandingUA_idle3",
            "HubStandingUA_move1",
            "HubStandingUA_move2"
        };

        hideWeapon = true;
    };
    class STAND_NO_WEAP_2 : Animation
    {
        moves[] = {
            "HubStandingUB_idle1",
            "HubStandingUB_idle2",
            "HubStandingUB_idle3",
            "HubStandingUB_move1"
        };

        hideWeapon = true;
    };
    class STAND_NO_WEAP_3 : Animation
    {
        moves[] = {
            "HubStandingUC_idle1",
            "HubStandingUC_idle2",
            "HubStandingUC_idle3",
            "HubStandingUC_move1",
            "HubStandingUC_move2"
        };

        hideWeapon = true;
    };
    class STAND_NO_WEAP_4 : Animation
    {
        moves[] = {
            "HubBriefing_think"
        };

        hideWeapon = true;
    };
    class STAND_NO_WEAP_5 : Animation
    {
        moves[] = {
            "Acts_AidlPercMstpSnonWnonDnon_warmup_1_loop",
            "Acts_AidlPercMstpSnonWnonDnon_warmup_2_loop", 
            "Acts_AidlPercMstpSnonWnonDnon_warmup_8_loop"
        };

        hideWeapon = true;
    };
    class STAND_GUARD_P1 : Animation
    {
        moves[] = {
            "Acts_AidlPercMstpSloWWpstDnon_warmup_1_loop"
        };
    };
    class STAND_GUARD_P2 : Animation
    {
        moves[] = {
            "Acts_AidlPercMstpSloWWpstDnon_warmup_2_loop"
        };
    };
    class STAND_GUARD_P3 : Animation
    {
        moves[] = {
            "Acts_AidlPercMstpSloWWpstDnon_warmup_3_loop"
        };
    };
    class STAND_GUARD_P4 : Animation
    {
        moves[] = {
            "Acts_AidlPercMstpSloWWpstDnon_warmup_6_loop"
        };
    };

    class WATCH_1 : Animation
    {
        moves[] = {
            "inbasemoves_patrolling1"
        };
    };
    class WATCH_2 : Animation
    {
        moves[] = {
            "inbasemoves_patrolling2"
        };
    };
    class GUARD : Animation
    {
        moves[] = {
            "inbasemoves_handsbehindback1",
            "inbasemoves_handsbehindback2"
        };

        hideWeapon = true;
    };

    class LISTEN_BRIEFING : Animation
    {
        moves[] = {
            "unaercposlechvelitele1",
            "unaercposlechvelitele2",
            "unaercposlechvelitele3",
            "unaercposlechvelitele4"
        };

        hideWeapon = true;
    };
    class BRIEFING : Animation
    {
        moves[] = {
            "hubbriefing_loop",
            "hubbriefing_loop",
            "hubbriefing_loop",
            "hubbriefing_lookaround1",
            "hubbriefing_lookaround2",
            "hubbriefing_scratch",
            "hubbriefing_stretch",
            "hubbriefing_talkaround"
        };

        hideWeapon = true;
    };
    class BRIEFING_INTERACTIVE_1 : Animation
    {
        moves[] = {
            "Acts_C_in1_briefing"
        };

        hideWeapon = true;
    };
    class BRIEFING_INTERACTIVE_2 : Animation
    {
        moves[] = {
            "Acts_HUBABriefing"
        };

        hideWeapon = true;
    };

    class LISTEN_TO_RADIO : Animation
    {
        moves[] = {
            "Acts_listeningToRadio_Loop"
        };
    };

    class NAVIGATE : Animation
    {
        moves[] = {
            "Acts_NavigatingChopper_Loop"
        };
    };

    class LEAN : Animation
    {
        moves[] = {
            "inbasemoves_lean1"
        };
    };

    class REPAIR_VEH_PRONE : Animation
    {
        moves[] = {
            "hubfixingvehicleprone_idle1"
        };

        hideWeapon = true;
    };
    class REPAIR_VEH_KNEEL : Animation
    {
        moves[] = {
            "inbasemoves_repairvehicleknl"
        };

        hideWeapon = true;
    };
    class REPAIR_VEH_STAND : Animation
    {
        moves[] = {
            "inbasemoves_assemblingvehicleerc"
        };

        hideWeapon = true;
    };

    class PRONE_INJURED_NO_WEAP_1 : Animation
    {
        moves[] = {
            "ainjppnemstpsnonwnondnon"
        };

        hideWeapon = true;
    };
    class PRONE_INJURED_NO_WEAP_2 : Animation
    {
        moves[] = {
            "hubwoundedprone_idle1",
            "hubwoundedprone_idle2"
        };

        hideWeapon = true;
    };
    class PRONE_INJURED : Animation
    {
        moves[] = {
            "acts_injuredangryrifle01",
            "acts_injuredcoughrifle02",
            "acts_injuredlookingrifle01",
            "acts_injuredlookingrifle02",
            "acts_injuredlookingrifle03",
            "acts_injuredlookingrifle04",
            "acts_injuredlookingrifle05",
            "acts_injuredlyingrifle01"
        };
    };
    class INJURY_CHEST : Animation
    {
        moves[] = {
            "Acts_CivilinjuredChest_1"
        };
    };
    class INJURY_HEAD : Animation
    {
        moves[] = {
            "Acts_CivilInjuredHead_1"
        };
    };
    class INJURY_ARM : Animation
    {
        moves[] = {
            "Acts_CivilInjuredArms_1"
        };
    };
    class INJURY_LEG : Animation
    {
        moves[] = {
            "Acts_CivilInjuredLegs_1"
        };
    };

    class KNEEL_TREAT_1 : Animation
    {
        moves[] = {
            "ainvpknlmstpsnonwnondnon_medic",
            "ainvpknlmstpsnonwnondnon_medic0",
            "ainvpknlmstpsnonwnondnon_medic1",
            "ainvpknlmstpsnonwnondnon_medic2",
            "ainvpknlmstpsnonwnondnon_medic3",
            "ainvpknlmstpsnonwnondnon_medic4",
            "ainvpknlmstpsnonwnondnon_medic5"
        };

        hideWeapon = true;
    };
    class KNEEL_TREAT_2 : Animation
    {
        moves[] = {
            "acts_treatingwounded01",
            "acts_treatingwounded02",
            "acts_treatingwounded03",
            "acts_treatingwounded04",
            "acts_treatingwounded05",
            "acts_treatingwounded06"
        };

        hideWeapon = true;
    };

    class CAPTURED_SIT : Animation
    {
        moves[] = {
            "Acts_AidlPsitMstpSsurWnonDnon03",
            "Acts_AidlPsitMstpSsurWnonDnon04",
            "Acts_AidlPsitMstpSsurWnonDnon05"
        };
    };
    
    class SIT_LOW_1 : Animation
    {
        moves[] = {
            "amovpsitmstpslowwrfldnon",
            "amovpsitmstpslowwrfldnon_weaponcheck1",
            "amovpsitmstpslowwrfldnon_weaponcheck2"
        };
    };
    class SIT_LOW_2 : Animation
    {
        moves[] = {
            "passenger_flatground_crosslegs"
        };
    };
    class SIT_LOW_3 : Animation
    {
        moves[] = {
            "Acts_passenger_flatground_leanright"
        };
    };
    class SIT_LOW_4 : Animation
    {
        moves[] = {
            "commander_sdv"
        };
    };
    class SIT_LOW_5 : Animation
    {
        moves[] = {
            "passenger_flatground_2_Idle_Unarmed"
        };
    };
    class SIT_LOW_6 : Animation
    {
        moves[] = {
            "passenger_flatground_3_Idle_Unarmed"
        };
    };
    class SIT_WEAP_1 : Animation
    {
        moves[] = {
            "passenger_flatground_1_Idle_Pistol_Idling"
        };
    };
    class SIT_WEAP_2 : Animation
    {
        moves[] = {
            "passenger_flatground_1_Idle_Pistol"
        };
    };
    class SIT_WEAP_3 : Animation
    {
        moves[] = {
            "passenger_flatground_1_Idle_Idling"
        };
    };
    class SIT_WEAP_4 : Animation
    {
        moves[] = {
            "passenger_flatground_3_Idle_Idling"
        };
    };
    class SQUAT : Animation
    {
        moves[] = {
            "Acts_AidlPercMstpSnonWnonDnon_warmup_4_loop"
        };
    };
    class SQUAT_WEAP : Animation
    {
        moves[] = {
            "Acts_AidlPercMstpSloWWrflDnon_warmup_6_loop"
        };
    };

    class CIV_HIDE : Animation
    {
        moves[] = {
            "Acts_CivilHiding_1",
            "Acts_CivilHiding_2"
        };

        hideWeapon = true;
    };
    class CIV_SHOCK : Animation
    {
        moves[] = {
            "Acts_CivilShocked_1",
            "Acts_CivilShocked_2"
        };

        hideWeapon = true;
    };

    class CIV_TALK : Animation
    {
        moves[] = {
            "Acts_CivilTalking_1",
            "Acts_CivilTalking_2"
        };

        hideWeapon = true;
    };
    class CIV_LISTEN : Animation
    {
        moves[] = {
            "Acts_CivilListening_1",
            "Acts_CivilListening_2"
        };

        hideWeapon = true;
    };

    class SHIELD_FROM_SUN : Animation
    {
        moves[] = {
            "Acts_ShieldFromSun_Loop"
        };
    };
    class SHOWING_THE_WAY : Animation
    {
        moves[] = {
            "Acts_ShowingTheRightWay_loop"
        };
    };

    class DEAD_LEAN_1 : Animation
    {
        moves[] = {
            "KIA_Commander_MBT_04"
        };

        hideWeapon = true;
    };
    class DEAD_LEAN_2 : Animation
    {
        moves[] = {
            "KIA_driver_MBT_04"
        };

        hideWeapon = true;
    };
    class DEAD_SIT_1 : Animation
    {
        moves[] = {
            "KIA_passenger_flatground"
        };

        hideWeapon = true;
    };
    class DEAD_SIT_2 : Animation
    {
        moves[] = {
            "KIA_passenger_sdv"
        };

        hideWeapon = true;
    };
    class DEAD_SIT_3 : Animation
    {
        moves[] = {
            "KIA_commander_sdv"
        };

        hideWeapon = true;
    };

    class KNEEL_WEAP_UP : Animation
    {
        moves[] = {
            "viper_crouchLoop", 
            "viperSgt_crouchLoop"
        };
    };

    class TABLE : Animation
    {
        moves[] = {
            "InBaseMoves_table1"
        };

        hideWeapon = true;
    };

    class BORED : Animation
    {
        moves[] = {
            "LHD_krajPaluby"
        };
    };

    class BINOC : Animation
    {
        moves[] = {
            "passenger_flatground_1_Aim_binoc"
        };
    };

    class FLASH : Animation
    {
        moves[] = {
            "Acts_CrouchingCoveringRifle01"
        };
    };
};