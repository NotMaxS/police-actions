class FlashLauncher
{
    class Flash
    {
        // Visually spawned ammo type, does not influence the actual explosion parameters
        ammoType = "ACE_G_M84";

        // Value by how much initial spawn velocity of flashbang is divided, used to prevent excessive bouncing
        spawnVelocityDivisor = 2.5;

        class Explosion
        {
            // Basic fuze time in seconds
            fuzeTime = 0.75;

            // Amount of explosions spawned per flash
            amount = 2;

            // Average interval between explosions in seconds
            avgInterval = 0.35;

            // Random deviation between explosions in seconds, random command input
            intervalDeviation[] = { -0.15, 0, 0.15 };

            // Sound pool to be picked for explosion
            sounds[] = {
                "A3\Sounds_F\arsenal\explosives\grenades\Explosion_HE_grenade_01.wss",
                "A3\Sounds_F\arsenal\explosives\grenades\Explosion_HE_grenade_02.wss",
                "A3\Sounds_F\arsenal\explosives\grenades\Explosion_HE_grenade_03.wss",
                "A3\Sounds_F\arsenal\explosives\grenades\Explosion_HE_grenade_04.wss"
            };

            // Played sound parameters; correspond to volume, pitch and audible distance
            soundParams[] = { 5, 1.2, 400 };
        };
    };

    class Carrier
    {
        // "Mule" ammo type carrying the flashbang to touchdown point
        // Ideally must be a bullet or non-explosive grenade
        ammoType = "B_556x45_Ball_Tracer_Green";

        visualObject = "\a3\weapons_f\ammo\ugl_slug.p3d"
    };
};