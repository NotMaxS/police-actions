class FlashAI
{
    class ACE
    {
        // Path of flashbang explosion EH script that was overriden
        scriptPath = "/z/ace/addons/grenades/functions/fnc_flashbangExplosionEH.sqf";
        
        // AI subskills affected by getting flashed
        aiSubskills[] = {
            "aimingAccuracy", "aimingShake", "aimingSpeed", "spotDistance", "spotTime", "courage", "reloadSpeed", "commanding", "general"
        };

        // Hard limit on amount of simultaneously flashed AIs to prevent crashing
        affectedUnitCap = 10;
    };
    
    class Animations
    {
        flash = "Acts_CrouchingCoveringRifle01";

        // Shifts to knee, then gets up from kneeling
        out[] = {
            "AmovPknlMstpSlowWrflDnon",
            "AmovPercMstpSrasWrflDnon"
        };
    };
    
    class Thresholds
    {
        // Flash strength required to apply any debuff effects
        min = 0.5;
        
        // Flash strength that will force unit to play flashed animation
        anim = 0.8;

        // Chance that flashed animation will play after reaching threshold, between [0; 1]
        animChance = 0.9;

        // Flash strenght that will momentarily knock unit unconscious
        knockout = 0.9;
    };

    // Knockout time for the unit, range
    knockoutTime[] = { 8.0, 12.5, 16.0 };

    // Max duration of flashed effect in seconds
    flashMaxDuration = 6;
};