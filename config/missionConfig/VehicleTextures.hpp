class VehicleTextures
{
    // Allows to apply mission-supplied textures to vehicles
    version = 1.0;
    
    /*
        Sample config:

        class B_Car_F
        {
            // Default (root) texture set; applied when requested texture set is not found
            hiddenSelectionTextures[] = {};
            hiddenSelectionMaterials[] = {};

            // Multiple texture set definitions are allowed
            // Each texture set can optionally specify it's weight value, it will be used if vehicle texture is randomly picked
            class VariantA
            {
                weight = 0.5;

                hiddenSelectionTextures[] = {};
                hiddenSelectionMaterials[] = {};
            };

            class VariantB
            {
                hiddenSelectionTextures[] = {};
                hiddenSelectionMaterials[] = {};
            };
        };
    */

    // Mission config
    class CUP_B_BTR60_CDF
    {
        // Root set
        hiddenSelectionTextures[] = {
            "\CUP\WheeledVehicles\CUP_WheeledVehicles_BTR60\data\textures\russia\russia_body_green_co.paa",
            "\CUP\WheeledVehicles\CUP_WheeledVehicles_BTR60\data\textures\russia\russia_detail_green_co.paa"
        };

        // Variants
        class Police
        {
            hiddenSelectionTextures[] = {
                "media\textures\btr60_body_cdf_co.paa",
                "media\textures\btr60_details_cdf_co.paa"
            };
        };
    };
};