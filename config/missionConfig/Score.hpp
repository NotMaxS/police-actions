class Score
{
    class Captive
    {
        // Score value granted for captured unit
        bonusScore = 50;

        // Units that can be captured by playerfor
        eligibleUnits[] = {
            "CUP_O_INS_Soldier_AA", 
            "CUP_O_INS_Soldier_Ammo", 
            "CUP_O_INS_Soldier_AT", 
            "CUP_O_INS_Soldier_AR",
            "CUP_O_INS_Story_Lopotev",
            "CUP_O_INS_Commander",
            "CUP_O_INS_Crew",
            "CUP_O_INS_Soldier_Engineer",
            "CUP_O_INS_Soldier_GL", 
            "CUP_O_INS_Story_Bardak", 
            "CUP_O_INS_Soldier_MG", 
            "CUP_O_INS_Medic", 
            "CUP_O_INS_Officer", 
            "CUP_O_INS_Pilot", 
            "CUP_O_INS_Soldier", 
            "CUP_O_INS_Soldier_AK74", 
            "CUP_O_INS_Soldier_LAT", 
            "CUP_O_INS_Saboteur", 
            "CUP_O_INS_Soldier_Exp", 
            "CUP_O_INS_Sniper", 
            "CUP_O_INS_Villager3", 
            "CUP_O_INS_Woodlander3", 
            "CUP_O_INS_Woodlander2", 
            "CUP_O_INS_Worker2", 
            "CUP_O_INS_Villager4", 
            "CUP_O_INS_Woodlander1", 
            "O_soldier_Melee"
        };
    };

    class Hostage
    {
        // Score value granted for captured unit
        bonusScore = 100;

        // Units considered hostages
        eligibleUnits[] = {
            "CUP_B_CDF_Officer_FST"
        };
    };

    class Civilian
    {
        // Score penalty for killing civilians
        penaltyScore = 100;
    };
};