class VehicleComposition
{
    version = 1.0;

    class FlashLauncher
    {
        compatibleVehicles[] = {"CUP_BTR60_Base"};
        supportVariants = true;
        
        class Turrets
        {
            class AGS
            {
                type = "CUP_B_AGS_CDF";
                init = "_this call MEX_fnc_flashLauncher_setup";
                canBeDisassembled = false;

                class Ammo
                {
                    // Array of turret path, magazines per path
                    magazinesToRemove[] = {
                        { {0}, {"CUP_29Rnd_30mm_AGS30_M"}}
                    };
                    
                    // Array of turret path, magazines per path
                    magazinesToAdd[] = {
                        { {0}, {"CUP_29Rnd_30mm_AGS30_M", "CUP_29Rnd_30mm_AGS30_M"} }
                    };
                };
                
                class SpecialStates
                {
                    isDamageEnabled = false;
                    isSimulationEnabled = true;
                };

                class Placement
                {
                    class CUP_BTR60_Base
                    {
                        position[] = {0.0336914,-0.688965,1.16339};
                        vectorDir[] = {3.69959e-005, 0.994997, 0.0999057};
                        vectorUp[] = {0.000277523,-0.0999057,0.994997};

                        blockCargo[] = {};
                        blockCrew[] = {};

                        // Moved - disable attachment to turret
                        // memPoint = "otocvez";
                    };
                };
            };
        };

        class Props 
        {
            class SandbagTop
            {
                type = "Land_BagFence_01_short_green_F";

                class SpecialStates
                {
                    isDamageEnabled = false;
                    isSimulationEnabled = false;
                };

                class Placement
                {
                    class CUP_BTR60_Base
                    {
                        position[] = {0.0478516, -0.59082, -0.0179734};
                        vectorDir[] = {-0.000275924, -0.00398496, -0.999992};
                        vectorUp[] = {-0.000451127, -0.999992, 0.00398508};

                        blockCargo[] = {{"turret", {8}}, {"turret", {9}}}; // i.e. 0, 1 or {"turret", {3}}, {"turret", {4, 5}}
                        blockCrew[] = {}; // i.e. {"gunner", {0}}

                        // memPoint = "";
                    };
                };
            };

            class SandbagLeftFront
            {
                type = "Land_BagFence_01_short_green_F";
                
                class SpecialStates
                {
                    isDamageEnabled = false;
                    isSimulationEnabled = false;
                };

                class Placement
                {
                    class CUP_BTR60_Base
                    {
                        position[] = {-1.07031, 0.47998, -0.520114};
                        vectorDir[] = {0.939579, 0.0348995, -0.340549};
                        vectorUp[] = {0.340757, -6.98492e-010, 0.940151};

                        blockCargo[] = {};
                        blockCrew[] = {};

                        // memPoint = "";
                    };
                };
            };

            class SandbagLeftBack
            {
                type = "Land_BagFence_01_short_green_F";
                
                class SpecialStates
                {
                    isDamageEnabled = false;
                    isSimulationEnabled = false;
                };

                class Placement
                {
                    class CUP_BTR60_Base
                    {
                        position[] = {-1.06738, -1.24072, -0.523114};
                        vectorDir[] = {0.939579, 0.0348995, -0.340549};
                        vectorUp[] = {0.340757, -6.98492e-010, 0.940151};

                        blockCargo[] = {};
                        blockCrew[] = {};

                        // memPoint = "";
                    };
                };
            };

            class SandbagRightFront
            {
                type = "Land_BagFence_01_short_green_F";
                
                class SpecialStates
                {
                    isDamageEnabled = false;
                    isSimulationEnabled = false;
                };

                class Placement
                {
                    class CUP_BTR60_Base
                    {
                        position[] = {1.04688, 0.471191, -0.533113};
                        vectorDir[] = {0.932592, 0.0348995, 0.35924};
                        vectorUp[] = {-0.359459, -6.98492e-010, 0.933161};

                        blockCargo[] = {};
                        blockCrew[] = {};

                        // memPoint = "";
                    };
                };
            };

            class SandbagRightBack
            {
                type = "Land_BagFence_01_short_green_F";
                
                class SpecialStates
                {
                    isDamageEnabled = false;
                    isSimulationEnabled = false;
                };

                class Placement
                {
                    class CUP_BTR60_Base
                    {
                        position[] = {1.06689,-1.21094,-0.502114};
                        vectorDir[] = {0.932592, 0.0348995, 0.35924};
                        vectorUp[] = {-0.359459, -6.98492e-010, 0.933161};

                        blockCargo[] = {};
                        blockCrew[] = {};

                        // memPoint = "";
                    };
                };
            };
        };
    };
};