// Mission endings
// Handles the mission ending screen
// https://community.bistudio.com/wiki/Debriefing


class totalVictory 
{
	title = "Total Victory"; // Main text that appears for the closing shot (ex: MISSION COMPLETED)
	subtitle = "Flawless Execution"; // Subtitle below the title when the closing shot is triggered
	description = "With this, a strong message has been sent.<br/>Chedaki are forced to go into hiding and captured insurgents are given long prison terms.<br/><br/>The Russian Forces are forced to stand down and return home."; // Description visible on the debriefing screen after the closing shot
	//pictureBackground = ""; // Image file used as a background on the debriefing screen
	//picture = ""; // Icon used for the closing shot
	//pictureColor[] = {1,1,1,1}; // Colour of the icon during the closing shot. Leave as default most of the time
};

class regularVictory 
{
	title = "Victory"; // Main text that appears for the closing shot (ex: MISSION COMPLETED)
	subtitle = "Solid execution and effort"; // Subtitle below the title when the closing shot is triggered
	description = "You made some mistakes but managed to pull through in the end.<br/>Pro-Russian media criticizes the Chernarussian response to the crisis but Chedaki forces are expelled from the city.<br/><br/>The Russian Forces stand down and return home."; // Description visible on the debriefing screen after the closing shot
	//pictureBackground = ""; // Image file used as a background on the debriefing screen
	//picture = ""; // Icon used for the closing shot
	//pictureColor[] = {1,1,1,1}; // Colour of the icon during the closing shot. Leave as default most of the time
};

class partialVictory 
{
	title = "Partial Victory"; // Main text that appears for the closing shot (ex: MISSION COMPLETED)
	subtitle = "Better than nothing performance"; // Subtitle below the title when the closing shot is triggered
	description = "While most of the city stays in Chernarussian hands, the Chedaki retain partial control and pro-Russian sentiment continues to foment in the city.<br/><br/>The future of Susice and similar border towns remains uncertain."; // Description visible on the debriefing screen after the closing shot
	//pictureBackground = ""; // Image file used as a background on the debriefing screen
	//picture = ""; // Icon used for the closing shot
	//pictureColor[] = {1,1,1,1}; // Colour of the icon during the closing shot. Leave as default most of the time
};

class youLost 
{
	title = "Loss"; // Main text that appears for the closing shot (ex: MISSION COMPLETED)
	subtitle = "Extract, there's nothing more you can do"; // Subtitle below the title when the closing shot is triggered
	description = "Your team pulls out, the Chedaki hold the city and shortly after the Russian Forces roll in.<br/>Together with the Chedaki, they are able to claim many Chernarussian border towns.<br/><br/>The fate of Chernarussian independence remains uncertain."; // Description visible on the debriefing screen after the closing shot
	//pictureBackground = ""; // Image file used as a background on the debriefing screen
	//picture = ""; // Icon used for the closing shot
	//pictureColor[] = {1,1,1,1}; // Colour of the icon during the closing shot. Leave as default most of the time
};