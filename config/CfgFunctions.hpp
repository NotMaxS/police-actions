

class SXP // Function TAG, used for the first part of the function name
{
	class vehicle // Function category, defines the folder that the file is located in
	{
		class setupVehicle {}; // Function class. Defines the file 
	};

}; 

class SLV
{
	class mission
	{
		class mission_getEndSummary {};
		class mission_onInit {preInit=1;};
		class mission_onMissionEnd {};
	};
	
	class score
	{
		class score_diag {};
		class score_getEnding {};
		class score_handleInitWagon {};
		class score_onAceKilled {};
		class score_onInit {preInit=1;};
		class score_onScoreUpdate {};
	};

	class scoreUI
	{
		class scoreUI_onInit {preInit=1;};
		class scoreUI_onKeyPress {};
		class scoreUI_onPenaltyApplied {};
	};
}; 

class MEX
{
	class feature_ambientAnim
	{
		class ambientAnim_aiDisable {};
		class ambientAnim_aiEnable {};
		class ambientAnim_onAnimate {};
		class ambientAnim_onPreInit { preInit=1; };
		class ambientAnim_onToggleAi {};
		class ambientAnim_start {};
		class ambientAnim_stop {};
		class ambientAnim_switchNextMove {};
	};


	class feature_flashAI
	{
		class flashAI_diag {};
		class flashAI_onExploded {};
		class flashAI_onExplodedAI {};
		class flashAI_onExplodedClient {};
		class flashAI_onFlashbangedAI {};
		class flashAI_onInit { preInit = 1; };
		class flashAI_playAnim {};
		class flashAI_resetAnim {};
	};

	class feature_flashLauncher
	{
		class flashLauncher_onFiredPlayerVehicle {};
		class flashLauncher_onInit { preInit=1; };
		class flashLauncher_onSetup {};
		class flashLauncher_onTearDown {};
		class flashLauncher_setup {};
		class flashLauncher_spawnFlash {};
		class flashLauncher_tearDown {};
	};

	class feature_stats
	{
		class stats_diag {};
		class stats_getStats {};
		class stats_onAceKilled {};
		class stats_onInit { preInit=1; }
		class stats_onStatsUpdate { preInit=1; }
	};

	class feature_vehicleComposition
	{
		class vehicleComposition_create {};
		class vehicleComposition_createObject {};
		class vehicleComposition_remove {};
	};

	class feature_vehicleTexture
	{
		class vehicleTexture_apply {};
		class vehicleTexture_applyRandom {};
	};
};
