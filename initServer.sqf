// initServer.sqf
// Executes only on the server at mission start
// No parameters are passed to this script

// Call the template initServer function
[] call XPT_fnc_initServer; // DO NOT CHANGE THIS LINE

// Call the script to handle initial task setup
[] execVM "scripts\tasks.sqf";

//////////////////////////////////////////////////////////
///// Add any mission specific code after this point /////
//////////////////////////////////////////////////////////

// Create a list of mission objects that should not be curator editable
XPT_blacklistedMissionObjects = [];


//Initialize our global variables then move them to public variable status in mission name space
SLV_endMissionVar = false;
missionNamespace setVariable ["SLV_endMissionVar", false, true];  

//time multiplier
setTimeMultiplier 0.1;
