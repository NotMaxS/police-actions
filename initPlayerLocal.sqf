// initPlayerLocal.sqf
// Executes on a client machine when they load the mission, regardless of if it's at mission start or JIP.
// _this = [player:Object, didJIP:Boolean]
params ["_player", "_jip"];

// Call the template initPlayerLocal function
_this call XPT_fnc_initPlayerLocal; // DO NOT CHANGE THIS LINE

// Add any mission specific code after this point

// Comment this line if you're looking to use a different method to set up briefings
[] execVM "scripts\briefing.sqf";

//Create hold action on the hold action radio object in order to end the mission 
[
	holdActionRadio,
	"Radio High Command",
	"\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_connect_ca.paa",
	"\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_connect_ca.paa",
	"_this distance _target < 3 && (SLV_endMissionVar == false)",
	"_caller distance _target < 3  && (SLV_endMissionVar == false)",
	{},
	{},
	{missionNameSpace setVariable ["SLV_endMissionVar", true, true];},
	{},
	[],
	1,
	0,
	false,
	false
] call BIS_fnc_holdActionAdd;

// Disable all lights on terrain
[] execVM "scripts\setup_terrain.sqf";

//Plays a local hint for the BTR-60 driver with helpful tooltip
player addEventHandler ["GetInMan", {
	params ["_unit", "_role", "_vehicle", "_turret"];

	if (_role == "driver" && typeOf _vehicle == "CUP_B_BTR60_CDF") then
	{	
		hint parseText "<t size='1.5' font='RobotoCondensedBold'>First person view</t><br/><br/>
			To move your camera closer to observation slits, use your <t font='RobotoCondensedBold'>Look</t> keys to look up and down.<br/><br/>
			Look keys by default are bound to Numpad, and can be found under <t font='RobotoCondensedBold'>Controls/View/Look</t><br/>";
	};
	
	player removeEventHandler [_thisEvent, _thisEventHandler];
}];