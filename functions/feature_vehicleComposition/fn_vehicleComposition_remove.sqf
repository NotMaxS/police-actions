/*
    MEX_fnc_vehicleComposition_remove
    Locality: Global effect, Server execution
    Author: erem2k

    Removes previously added composition from the vehicle.
        Called when vehicle is destroyed, or by mission script.

    Parameters:
        0 - Object, vehicle
            Vehicle with composition previously attached to it.

    Returns:
        None
*/

params[
    ["_vehicle", objNull, [objNull]]
];

if (!isServer) exitWith {};

if (isNull _vehicle) exitWith {
    ["error", "Attempting to remove composition from null vehicle"] call TEX_fnc_log;
};

private _attachedObjects = _vehicle getVariable ["MEX_vehicleComposition_attachedObjects", []];

if (count _attachedObjects <= 0) exitWith {
    ["error", "Vehicle %1 has no compositions attached, skipping"] call TEX_fnc_log;
};

// Detach event handlers
private _turretEhs = _vehicle getVariable ["MEX_vehicleComposition_turret_eventHandlers", []];

{
    private _ehType = _x param [0, "", [""]];
    private _ehId = _x param [1, -1, [0]];

    _vehicle removeEventHandler [_ehType, _ehId];

} forEach (_turretEhs);

_vehicle setVariable ["MEX_vehicleComposition_turret_eventHandlers", nil, true];

// Finally, reset the composition
{
    private _attachedObject = _x;
    
    if (isNull _attachedObject) then {
        continue;
    };

    // If players are present in a turret, boot them out
    if (_attachedObject isKindOf "StaticWeapon") then {

        private _turretCrew = crew _attachedObject;
        {
            moveOut _x;
        } forEach _turretCrew;
    };

    detach _attachedObject;
    deleteVehicle _attachedObject;

} forEach _attachedObjects;