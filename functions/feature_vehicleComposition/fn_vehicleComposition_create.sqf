/*
    MEX_fnc_vehicleComposition_create
    Locality: Global effect, Server execution
    Author: erem2k

    Attempts to create requested object composition on a given vehicle.

    Parameters:
        0 - Object, vehicle
            Composition will be created and attached to this vehicle; vehicle must be compatible with composition.
        
        1 - String, composition name
            Must correspond to one of compositions defined in VehicleComposition config.

    Returns:
        None
*/

params[
    ["_vehicle", objNull, [objNull]], 
    ["_compName", "", [""]]
];

if (!isServer) exitWith {};

if (isNull _vehicle) exitWith {
    ["error", "(%1) Trying to apply composition to a null vehicle!", _compName] call TEX_fnc_log;
};

private _compInfo = [["VehicleComposition", _compName], configNull] call TEX_fnc_cfg_getOrDefault;

if (_compInfo isEqualTo configNull) exitWith {
    ["error", "(%1) Composition not found, applied to %2", _compName, typeOf _vehicle] call TEX_fnc_log;
};

// Check if composition supports the given vehicle
private _variantsSupported = _compInfo getOrDefault ["supportVariants", false];
private _compatibleVehicles = _compInfo getOrDefault ["compatibleVehicles", []];
private _isVehicleSupported = false;

_isVehicleSupported = if (_variantsSupported) then {
    (_compatibleVehicles findIf { _vehicle isKindOf _x }) >= 0
} else {
    (_compatibleVehicles findIf { typeOf _vehicle == _x }) >= 0
};

if (!_isVehicleSupported) exitWith {
    ["error", "(%1) Vehicle %2 is not supported by composition, aborting", _compName, typeOf _vehicle] call TEX_fnc_log;
};

// Start attaching composition
private _attachedObjects = [];
private _blockedCargoSeats = [];
private _blockedCrewSeats = [];

// Deploy turrets
private _turretsInfo = [_compInfo get "Turrets", createHashMap] call TEX_fnc_cfg_getOrDefault;
{
    private _attachedTurret = [_vehicle, [_x, _y], _variantsSupported] call MEX_fnc_vehicleComposition_createObject;

    if (isNull (_attachedTurret select 0)) then {
        ["error", "(%1) Failed to attach object %2 to vehicle %2, skipping", _compName, _x, typeOf _vehicle] call TEX_fnc_log;

        continue;
    };

    // Update blocked seats
    {
        _blockedCargoSeats pushBackUnique _x;
    } forEach (_attachedTurret select 1);

    {
        _blockedCrewSeats pushBackUnique _x;
    } forEach (_attachedTurret select 2);

    // Dump it in attached object pool
    _attachedObjects pushBack (_attachedTurret select 0);

} forEach _turretsInfo;

// Deploy props
private _propsInfo = [_compInfo get "Props", createHashMap] call TEX_fnc_cfg_getOrDefault;
{
    private _attachedProp = [_vehicle, [_x, _y], _variantsSupported] call MEX_fnc_vehicleComposition_createObject;

    if (isNull (_attachedProp select 0)) then {
        [
            "error", 
            "(%1) Failed to attach object %2 to vehicle %2, skipping",
            _compName,
            _x,
            typeOf _vehicle
        ] call TEX_fnc_log;

        continue;
    };

    // Update blocked seats
    {
        _blockedCargoSeats pushBackUnique _x;
    } forEach (_attachedProp select 1);

    {
        _blockedCrewSeats pushBackUnique _x;
    } forEach (_attachedProp select 2);

    // Dump it in attached object pool
    _attachedObjects pushBack (_attachedProp select 0);

} forEach _propsInfo;

// Lock requested cargo and crew seats
{
    // FFV turret edge case
    if (_x isEqualType []) then {
        _vehicle lockTurret [_x select 1, true];
        continue;
    };
    
    _vehicle lockCargo [_x, true];
} forEach _blockedCargoSeats;

{
    private _seatInfo = _x;
    
    // Driver edge case
    if (_seatInfo IsEqualTo "driver") then {
        _vehicle lockDriver true;
        continue;
    };

    // Rest of turret seats
    switch (_seatInfo select 0) do {
        case "gunner";
        case "commander":
        {
            _vehicle lockTurret [_seatInfo select 1, true]
        };
        default
        {
            ["error", "(%1) Unknown crew lock requested: %2", _compName, _seatInfo] call TEX_fnc_log;
        };
    };
} forEach _blockedCrewSeats;

// Set attached objects variable
_vehicle setVariable ["MEX_vehicleComposition_attachedObjects", _attachedObjects, true];

// Finally, set up veh state polling to remove composition when it gets destroyed
// While Killed EH is an option, it will need to exist on every machine as vehicle will change locality
[
    {
        params["_vehicle"];

        !alive _vehicle;
    },
    {
        params["_vehicle"];

        [_vehicle] call MEX_fnc_vehicleComposition_remove; 
    },
    [_vehicle]
] call CBA_fnc_waitUntilAndExecute;
