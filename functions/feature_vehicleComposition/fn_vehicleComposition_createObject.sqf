/*
    MEX_fnc_vehicleComposition_createObject
    Locality: Global effect
    Author: erem2k

    Creates object that's part of composition based on passed info.
        Called from MEX_fnc_vehicleComposition_create

    Parameters:
        0 - Object, vehicle
            Object will be created and attached to this vehicle; vehicle must be compatible with composition.
        
        1 - Array, String and Config
            Object's config class name and value from VehicleCompositions.

    Returns:
        None
*/

params[
    "_vehicle", 
    "_attachedObjectInfo",
    "_variantsSupported"
];

private _className = _attachedObjectInfo param [0, "", [""]];
private _classConfig = _attachedObjectInfo param [1, configNull, [configNull]];

if (_className == "" || _classConfig isEqualTo configNull) exitWith {
    [objNull, [], []]
};

// Find placement info map for object
private _objectInfo = (_classConfig) call TEX_fnc_cfg_get;
private _objectPosInfo = [((_objectInfo get "Placement") >> typeOf _vehicle), configNull] call TEX_fnc_cfg_getOrDefault;

// If we didn't find it, and we're in subclass support mode - iterate over config and find parent veh type
if (_variantsSupported && _objectPosInfo isEqualTo configNull) then {
    
    private _placementInfo = (_objectInfo get "Placement") call TEX_fnc_cfg_get;
    
    {
        if (_vehicle isKindOf _x) then {
            
            _objectPosInfo = (_y) call TEX_fnc_cfg_get;
            break;
        };
    } forEach (_placementInfo);
};

if (_objectPosInfo isEqualTo configNull) exitWith {

    ["error", "Missing object position info for %1, vehicle %2", _className, typeOf _vehicle] call TEX_fnc_log;
    [objNull, [], []]
};

// Attach the object
private _attachedObject = createVehicle [_objectInfo get "type", [0, 0, 0]];

_attachedObject attachTo [_vehicle, _objectPosInfo get "position", _objectPosInfo getOrDefault ["memPoint", ""], true];
_attachedObject setVectorDirAndUp [_objectPosInfo get "vectorDir",  _objectPosInfo get "vectorUp"];

private _objectSpecialStates = [_objectInfo get "SpecialStates", createHashMap] call TEX_fnc_cfg_getOrDefault;

// Apply special states
{
    private _specialStateName = _x;
    private _specialStateValue = _y;

    switch (_specialStateName) do {
        case "isDamageEnabled":
        {
            // local EH on every machine, reapplying it

            [_attachedObject, _specialStateValue] remoteExec ["allowDamage", 0, true];
        };
        case "isSimulationEnabled":
        {
            _attachedObject enableSimulationGlobal _specialStateValue;
        };
        case "isDynamicSimulationEnabled":
        {
            [_attachedObject, _specialStateValue] remoteExec ["enableDynamicSimulation", 0, true];
        };
    };
} forEach _objectSpecialStates;

if (_attachedObject isKindOf "StaticWeapon") then {

    // Run init code if requested 
    private _fn_initCallback = compile (_objectInfo getOrDefault ["init", "{}"]);
    [_attachedObject, _vehicle] call _fn_initCallback;

    // Block features
    _attachedObject enableWeaponDisassembly (_objectInfo get "canBeDisassembled");

    // Handle ammo if requested by config
    private _ammoInfo = [_objectInfo get "Ammo", createHashMap] call TEX_fnc_cfg_getOrDefault;

    {
        private _turretPath = _x param [0, [-1], [[]]];
        private _mags = _x param [1, []];

        {
            _attachedObject removeMagazinesTurret [_x, _turretPath];
        } forEach _mags;

    } forEach (_ammoInfo getOrDefault ["magazinesToRemove", []]);

    {
        private _turretPath = _x param [0, [-1], [[]]];
        private _mags = _x param [1, []];

        {
            _attachedObject addMagazineTurret [_x, _turretPath];
        } forEach _mags;

    } forEach (_ammoInfo getOrDefault ["magazinesToAdd", []]);

    // Be extra safe and disable collisions, because Arma
    _attachedObject disableCollisionWith _vehicle;

    // Fix locality issues
    // Different parts of the vehicle will be localized between different players in the wild
    // This is an attempt to fix potential collisions with parts of carrier vehicle by reapplying collision rules every time locality might change
    private _handlerId = -1;
    private _attachedHandlers = _vehicle getVariable ["MEX_vehicleComposition_turret_eventHandlers", []];

    // Turret handlers - don't track them, as on teardown it's deleted anyway
    [
        _attachedObject,
        "GetIn",
        {
            params ["_attachedTurret", "_role", "_unit", "_turret"];
            private _carrierVehicle = _thisArgs param [0, objNull, [objNull]];

            [_attachedTurret, _carrierVehicle] remoteExec ["disableCollisionWith", 0];    
        },
        [_vehicle]
    ] call CBA_fnc_addBISEventHandler;

    // Vehicle handlers
    // We are interested in switches to driver or gunner seat, or gunner on turret
    _handlerId = [
        _vehicle,
        "GetIn",
        {
            params ["_vehicle", "_role", "_unit", "_turret"];
            private _attachedTurret = _thisArgs param [0, objNull, [objNull]];

            if (not (_role in ["driver", "gunner"])) exitWith {};

            [_attachedTurret, _vehicle] remoteExec ["disableCollisionWith", 0];
        },
        [_attachedObject]
    ] call CBA_fnc_addBISEventHandler;

    _attachedHandlers pushBack ["GetIn", _handlerId];

    _handlerId = [
        _vehicle,
        "SeatSwitched",
        {
            params ["_vehicle", "_unit1", "_unit2"];
            private _attachedTurret = _thisArgs param [0, objNull, [objNull]];

            private _roles = [
                (assignedVehicleRole _unit1) param [0, "none"],
                (assignedVehicleRole _unit2) param [0, "none"]
            ];

            if (["driver", "turret"] findIf { _x in _roles} == -1) exitWith {};

            [_attachedTurret, _vehicle] remoteExec ["disableCollisionWith", 0];
        },
        [_attachedObject]
    ] call CBA_fnc_addBISEventHandler;

    _attachedHandlers pushBack ["SeatSwitched", _handlerId];
    _vehicle setVariable ["MEX_vehicleComposition_turret_eventHandlers", _attachedHandlers, true];
};

// Block features
[_attachedObject, false] remoteExec ["ace_dragging_fnc_setDraggable", 0, true];
[_attachedObject, false] remoteExec ["ace_dragging_fnc_setCarryable", 0, true];
[_attachedObject, -1] remoteExec ["ace_cargo_fnc_setSize", 0, true];

[_attachedObject, _objectPosInfo getOrDefault ["blockCargo", []], _objectPosInfo getOrDefault ["blockCrew", []]]