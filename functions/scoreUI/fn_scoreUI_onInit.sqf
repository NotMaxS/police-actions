/*
	SLV_fnc_scoreUI_onInit
	Locality: Local effect
	Author: NotMax
	
    Attaches event handlers required for function of scoreUI module.

	Parameters:
        None

	Returns:
        None
*/

// Player-activated score hint
[
    49, 
    [false, false, false], 
    {_this call SLV_fnc_scoreUI_onKeyPress}, 
    "keydown", 
    "" , 
    false
] call CBA_fnc_addKeyHandler;


// Penalty hint
[
    "SLV_score_penaltyApplied", 
    {_this call SLV_fnc_scoreUI_onPenaltyApplied;}
] call CBA_fnc_addEventHandler;