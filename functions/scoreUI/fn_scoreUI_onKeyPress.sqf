/*
	SLV_fnc_scoreUI_onKeyPress
	Locality: Local effect
	Author: NotMax
	
    Shows current score value, called back from keypress.

	Parameters:
        None

	Returns:
        None
*/

//Get current and display it in a formatted hint local to the player who is requesting it 

private _displayedScoreOnKeyPress = missionNameSpace getVariable ["SLV_playerScoreVar", 0]; 

hintSilent format ["Current score: %1", _displayedScoreOnKeyPress];

