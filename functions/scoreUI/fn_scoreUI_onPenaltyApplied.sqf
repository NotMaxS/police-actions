/*
	SLV_fnc_scoreUI_onPenaltyApplied
	Locality: Local effect
	Author: NotMax
	
    Displays a hint to the user indicating penalty was applied to global score value.

	Parameters:
        0 - String, penalty type
            Score event that applied the penalty

	Returns:
        None
*/
params["_penaltyType"];

if (_penaltyType == "civilianKilled") then {
    hintSilent "You have killed a civilian";
};
