/*
    MEX_fnc_flashLauncher_onTearDown
    Locality: Local effect
    Author: erem2k

    Tears down client and server event handlers for static flashbang grenade launcher.
        Called back from MEX_flashLauncher_tearDown event.

    Parameters:
        0 - Object, static grenade launcher
            Previously set up in MEX_fnc_flashLauncher_setup.

    Returns:
        None
*/

params[
    ["_launcher", objNull, [objNull]]
];

if (isNull _launcher) exitWith {};

private _attachedHandlers = _launcher getVariable ["MEX_flashLauncher_eventHandlers", []];

if (count _attachedHandlers <= 0) exitWith {
    ["warning", "Attempting to tear down launcher (%1-%2) that wasn't set up by flashLauncher module, aborting", typeOf _launcher, _launcher] call TEX_fnc_log;
};

{
    private _handlerType = _x params [0, ""];
    private _handlerId = _x params [1, 0];

    switch (true) do {
        case ((_handlerType find "BIS_") == 0):
        {
            _launcher removeEventHandler [_handlerType select [4], _handlerId];
        };
        case ((_handlerType find "CBA_") == 0):
        {
            [
                _handlerType select [4],
                _handlerId
            ] call CBA_fnc_removeEventHandler;
        };
    };
} forEach _attachedHandlers;

if (isServer) then {
    _launcher setVariable ["MEX_flashLauncher_isLauncher", false, true];
    _launcher setVariable ["MEX_flashLauncher_carrierVehicle", nil, true]
};