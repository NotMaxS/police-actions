/*
    MEX_fnc_flashLauncher_onSetup
    Locality: Local effect
    Author: erem2k

    Sets up clieant and server event handlers for static flashbang grenade launcher.
        Called back from MEX_flashLauncher_setup event.

    Parameters:
        0 - Object, static grenade launcher

    Returns:
        None
*/

params [
    ["_launcher", objNull, [objNull]]
];

if (isNull _launcher) exitWith {};
if (not alive _launcher) exitWith {};

// Attach EHs
private _eventHandlers = [];
private _ehId = 0;

_ehId = [
    "ace_firedPlayerVehicle",
    {
        _this params["_vehicle"];
        _thisArgs params["_flashLauncher"];

        if (not (_flashLauncher isEqualTo _vehicle)) exitWith {};

        _this call MEX_fnc_flashLauncher_onFiredPlayerVehicle;
    },
    [_launcher]
] call CBA_fnc_addEventHandlerArgs;

_eventHandlers pushBack ["CBA_ace_firedPlayerVehicle", _ehId];

_ehId = _launcher addEventHandler [
    "Killed",
    {
        params ["_unit", "_killer", "_instigator", "_useEffects"];

        // Local only to this machine, so broadcast update everywhere else
        ["MEX_flashLauncher_tearDownClient", [_unit]] call CBA_fnc_globalEventJIP;
    }
];

_eventHandlers pushBack ["BIS_Killed", _ehId];

_launcher setVariable ["MEX_flashLauncher_eventHandlers", _eventHandlers];