/*
    MEX_fnc_flashLauncher_onFired
    Locality: Global effect
    Author: erem2k

    Replaces projectile with carrier projectile inheriting it's position and velocity.
        At the end of travel live flashbang is spawned.
    
        Called back from BIS Fired EH.

    Parameters:
        0 - Object, unit
            Unit that fired the GL projectile, can be vehicle.

        1 - String, weapon classname
            Weapon fired.
        
        2 - String, muzzle classname
            Weapon's muzzle sued to spawn projectile.
        
        3 - String, fired weapon mode

        4 - String, ammo type used

        5 - String, magazine type used

        6 - Object, projectile
            Freshly spawned projectile of following ammo type.

        7 - Object, gunner unit
            Present if EH is attached to the vehicle.

    Returns:
        None
*/

params ["_vehicle", "_weapon", "_muzzle", "_mode", "_ammo", "_magazine", "_projectile"];

private _projectilePos = getPosASL _projectile;
private _projectileVelocity = velocity _projectile;

// HitPart on projectiles works with GL grenades, bullets, missiles, etc. so we can't just spawn flash grenade
// It also does not allow to delete the projectile, so we can't hook into it outright either
// So - we spawn a small caliber carrier projectile, and then spawn a flash when it collides with something once

private _carrierCfg =  ["FlashLauncher", "Carrier"] call TEX_fnc_cfg_get;
private _carrierProjectile = createVehicle [_carrierCfg get "ammoType", _projectilePos, [], 0, "CAN_COLLIDE"];

if (isNull _carrierProjectile) exitWith {
    ["error", "Projectile spawn failed! Firing from %1, gunner %2", _weapon, [] call ACE_common_fnc_player] call TEX_fnc_log_local;
    deleteVehicle _projectile;
};

[_carrierProjectile, getShotParents _projectile] remoteExec ["setShotParents", 2, false];

_carrierProjectile setPosASL _projectilePos;
_carrierProjectile setVelocity _projectileVelocity;
_carrierProjectile setDir (getDir _projectile);

// Visual 40MM slug, concealing the bullet
private _visualProjectile = createSimpleObject [_carrierCfg get "visualObject", _projectilePos];
_visualProjectile attachTo [_carrierProjectile, [0, 0, 0]];
_visualProjectile setVectorDirAndUp [[0,1,0],[0,0,1]];

// Handle flashbang subprojectile
_carrierProjectile addEventHandler [
    "HitPart",
    {
        params ["_projectile", "_hitEntity", "_projectileOwner", "_pos", "_velocity", "_normal", "_components", "_radius" ,"_surfaceType", "_instigator"];
        
        [_projectile, _pos, _velocity] call MEX_fnc_flashLauncher_spawnFlash;

        _projectile removeEventHandler ["HitPart", _thisEventHandler];
    }
];

// Handle visual projectile
_carrierProjectile addEventHandler [
    "Deleted",
    {
	    params ["_projectile"];
        
        {
            deleteVehicle _x;
        } forEach attachedObjects _projectile;
    }
];

// Handle collisions
private _launcherCarrierVehicle = _vehicle getVariable ["", objNull];
[_carrierProjectile, _vehicle] remoteExec ["disableCollisionWith", _vehicle];

if (not (isNull _launcherCarrierVehicle)) then {
    [_carrierProjectile, _launcherCarrierVehicle] remoteExec ["disableCollisionWith", _launcherCarrierVehicle];
};

// Finally, remove the projectile
deleteVehicle _projectile;