/*
    MEX_fnc_flashLauncher_spawnFlash
    Locality: Global effect
    Author: erem2k

    Spawns flashbang at the end of travel of carrier projectile.
        Then, sets up the fuze and initiates detonation.

    Parameters:
        0 - Object, carrier projectile object
            Typically a bullet, etc.

        1 - Array, carrier projectile position
            Format PositionASL.
        
        2 - Array, carrier projectile velocity
            Format Vector3D.

    Returns:
        None
*/

params ["_carrierProjectile", "_carrierProjectilePos", "_carrierProjectileVelocity"];

private _flashConfig = ["FlashLauncher", "Flash"] call TEX_fnc_cfg_get;

// Spawn flashbang, reduce velocity to discourage bouncing
private _velReduce = _flashConfig get "spawnVelocityDivisor";
private _flashVelocity = [(_carrierProjectileVelocity select 0) / _velReduce, (_carrierProjectileVelocity select 1) / _velReduce, (_carrierProjectileVelocity select 2) / _velReduce];

private _flashProjectile = createVehicle [_flashConfig get "ammoType", _carrierProjectilePos, [], 0, "CAN_COLLIDE"];

_flashProjectile setPosASL _carrierProjectilePos;
_flashProjectile setVelocity _flashVelocity;
_flashProjectile setVectorDirAndUp [[0,1,0], [1,0,0]];

[_flashProjectile, getShotParents _carrierProjectile] remoteExec ["setShotParents", 2, false];

// Lifted from ACE flashbang code - just spawning flashbang won't work, as they trigger it exclusively on hand throw
private _expConfig = ["FlashLauncher", "Flash", "Explosion"] call TEX_fnc_cfg_get;

private _explosionCount = 0;
while { _explosionCount < (_expConfig get "amount") } do {

    private _deviation = random (_expConfig get "intervalDeviation");
    private _delay = (_expConfig get "fuzeTime") + _explosionCount * (_expConfig get "avgInterval") + _deviation;

    [
        {
            params["_projectile", "_cfg"];

            private _posASL = getPosASL _projectile;

            private _file = selectRandom (_cfg get "sounds");
            (_cfg get "soundParams") params ["_volume", "_pitch", "_distance"];

            playSound3D [_file, _projectile, false, _posASL, _volume, _pitch, _distance];

            ["ace_flashbangExploded", [_posASL]] call CBA_fnc_globalEvent;
        }, 
        [_flashProjectile, _expConfig], 
        _delay

    ] call CBA_fnc_waitAndExecute;

    _explosionCount = _explosionCount + 1;
};
