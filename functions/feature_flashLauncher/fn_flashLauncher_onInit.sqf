/*
    MEX_fnc_flashLauncher_onInit
    Locality: Local effect
    Author: erem2k

    Sets up event handlers on init required for function of flashLauncher module.
        Ran during preInit.
    
    Parameters:
        None

    Returns:
        None
*/

// *_setup and *_teardown involve both server-side and client-side processing, so we need to listen in on both
[
    "MEX_flashLauncher_setUp",
    {
        _this call MEX_fnc_flashLauncher_onSetup;
    }
] call CBA_fnc_addEventHandler;

[
    "MEX_flashLauncher_tearDown",
    {
        _this call MEX_fnc_flashLauncher_onTearDown;
    }
] call CBA_fnc_addEventHandler;