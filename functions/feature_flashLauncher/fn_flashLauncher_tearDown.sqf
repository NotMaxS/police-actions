/*
    MEX_fnc_flashLauncher_tearDown
    Locality: Global effect
    Author: erem2k

    Removes previously set up ability for static grenade launcher to fire flashbangs.

    Parameters:
        0 - Object, static grenade launcher
            Set up earlier with MEX_fnc_flashLauncher_setup

    Returns:
        None
*/

params [
    ["_launcher", objNull, [objNull]]
];

if (isNull _launcher) exitWith {
    ["error", "Grenade launcher vehicle is null, aborting"] call TEX_fnc_log;
};

private _isFlashLauncher = _launcher getVariable ["MEX_flashLauncher_isLauncher", false];

if (!_isFlashLauncher) exitWith {
    ["error", "Grenade launcher %1-%2 was not previously set up to use flashbangs, aborting", typeOf _launcher, _launcher] call TEX_fnc_log;
};

// Notify client and server to tear down their event handlers
["MEX_flashLauncher_tearDown", [_launcher]] call CBA_fnc_globalEventJIP;