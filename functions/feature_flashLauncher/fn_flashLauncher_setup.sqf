/*
    MEX_fnc_flashLauncher_setup
    Locality: Global effect
    Author: erem2k

    Sets up static grenade launcher to fire flashbangs.

    Parameters:
        0 - Object, static grenade launcher
            Any static GL is supported.

    Returns:
        None
*/

params [
    ["_launcher", objNull, [objNull]],
    ["_carrierVehicle", objNull, [objNull]]
];

if (!isServer) exitWith {};

if (isNull _launcher) exitWith {
    ["error", "Grenade launcher vehicle is null, aborting"] call TEX_fnc_log;
};

if (!(_launcher isKindOf "StaticGrenadeLauncher")) exitWith {
    ["error", "Passed vehicle %1 is not a grenade launcher, aborting", typeOf _launcher] call TEX_fnc_log;
};

// Mark our boy
_launcher setVariable ["MEX_flashLauncher_isLauncher", true, true];

if (not (isNull _carrierVehicle)) then {
    _launcher setVariable ["MEX_flashLauncher_carrierVehicle", _carrierVehicle, true];
};

// Relevant events will be partially shared between server and client, this will notify both
[
    "MEX_flashLauncher_setUp",
    [_launcher]
] call CBA_fnc_globalEventJIP;