/*
	MEX_fnc_flashAI_onInit
	Locality: Local effect
	Author: erem2k
	
    Sets up event handlers required for flashAI module to function.

	Parameters:
        None

	Returns:
        None
*/

[
    "ace_grenades_flashbangedAI",
    {
        _this call MEX_fnc_flashAI_onFlashbangedAI;
    }
] call CBA_fnc_addEventHandler;

[
    "CAManBase",
    "AnimDone",
    {
        private _unit = (_this param [0, objNull, [objNull]]);
        
        [_unit] remoteExec ["MEX_fnc_flashAI_resetAnim", _unit];
    }
] call CBA_fnc_addClassEventHandler;

[
    "CAManBase",
    "Killed",
    {
        private _unit = (_this param [0, objNull, [objNull]]);

        [_unit] remoteExec ["MEX_fnc_flashAI_resetAnim", _unit];
    }
] call CBA_fnc_addClassEventHandler;

//////////////// Crash fix hackery ////////////////
// Override ACE flashbang exploded handler, essentially replacing mod's behavior
[
    {
        (count (CBA_events_eventNamespace getVariable ["ace_flashbangExploded", []])) > 0
    },
    {
        private _ehCallbacks = CBA_events_eventNamespace getVariable ["ace_flashbangExploded", []];
        private _overrideSignature = ["FlashAI", "ACE", "scriptPath"] call TEX_fnc_cfg_get;

        private _foundIndex = _ehCallbacks findIf { _overrideSignature in str(_x) };

        if (_foundIndex >= 0) then {
            ["ace_flashbangExploded", _foundIndex] call CBA_fnc_removeEventHandler;
        };

        // Apply the override; it must replicate or at least mimic original's logic
        ["ace_flashbangExploded", {_this call MEX_fnc_flashAI_onExploded}] call CBA_fnc_addEventHandler;

    }
] call CBA_fnc_waitUntilAndExecute;
