/*
	MEX_fnc_flashAI_playAnim
	Locality: Global effect
	Author: erem2k
	
    Plays flashbanged animation on a unit.

	Parameters:
        0 - Object, AI unit
            Flashbanged AI unit.

	Returns:
        None
*/

params [
    ["_unit", objNull, [objNull]]
];

if (isNull _unit) exitWith {};

private _anim = ["FlashAI", "Animations", "flash"] call TEX_fnc_cfg_get;

_unit setVariable ["MEX_flashAI_isAnimationPlayed", true, true];
[_unit, _anim] remoteExec ["switchMove", 0];
