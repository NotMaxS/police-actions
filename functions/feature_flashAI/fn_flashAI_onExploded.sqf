/*
	MEX_fnc_flashAI_onExploded
	Locality: Global effect
	Author: KoffeinFlummi, erem2k
 
    KoffeinFlummi: Creates the flashbang effect and knock out AI units.
	
    erem2k: ACE's implementation moved into onExplodedAI and onExplodedClient for easier handling.
        Simultaneously affected AI count is capped by mission config value to prevent crashes.

	Parameters:
        0 - Array, Position ASL
            Flashbang explosion position.

	Returns:
        None
*/

params ["_grenadePosASL"];

// Affect local AI (players are not local, except for ACE_player)
// @todo: Affect units in static weapons, turned out, etc

private _affectedAICount = missionNamespace getVariable ["MEX_flashAI_affectedUnitCount", 0];

// Limit unit count to avoid potential crashes
if (_affectedAICount <= ([["FlashAI", "ACE", "affectedUnitCap"], 10] call TEX_fnc_cfg_getOrDefault)) then {
    _this call MEX_fnc_flashAI_onExplodedAI;
};

if (hasInterface) then {
    _this call MEX_fnc_flashAI_onExplodedClient;
};
