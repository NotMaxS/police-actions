/*
	MEX_fnc_flashAI_onExplodedClient
	Locality: Global effect
	Author: KoffeinFlummi
 
    Creates the flashbang effect, blinds local player and applies ear ringing if required.

	Parameters:
        0 - Array, Position ASL
            Flashbang explosion position.

	Returns:
        None
*/

params ["_grenadePosASL"];

// Create flash to illuminate environment
private _light = "#lightpoint" createVehicleLocal ASLtoAGL _grenadePosASL;
_light setPosASL _grenadePosASL;

_light setLightBrightness 20;
_light setLightAmbient [1,1,1];
_light setLightColor [1,1,1];
_light setLightDayLight true;
_light setLightAttenuation [0, 1, 5, 1000, 0, 20];

// Reduce the light after 0.1 seconds
[{
    _this setLightBrightness 5;

    // Delete the light after 0.2 more seconds
    [{deleteVehicle _this}, _this, 0.2] call CBA_fnc_waitAndExecute;
}, _light, 0.1] call CBA_fnc_waitAndExecute;

// Ignore dead and placeable logic (zeus / spectator)
if (!alive ACE_player || {(getNumber (configOf ACE_player >> "isPlayableLogic")) == 1}) exitWith {};

// Affect local player, independently of distance
// Check for line of sight to the grenade
private _eyePos = eyePos ACE_player; // PositionASL
_grenadePosASL set [2, (_grenadePosASL select 2) + 0.2]; // compensate for grenade glitching into ground

private _strength = 1 - ((_eyePos vectorDistance _grenadePosASL) min 20) / 20;

// Check for line of sight (check 4 points in case grenade is stuck in an object or underground)
private _losCount = {
    !lineIntersects [_grenadePosASL vectorAdd _x, _eyePos, ACE_player]
} count [[0, 0, 0], [0, 0, 0.2], [0.1, 0.1, 0.1], [-0.1, -0.1, 0.1]];

private _losCoefficient = [1, 0.1] select (_losCount <= 1);
_strength = _strength * _losCoefficient;

// Add ace_hearing ear ringing sound effect
if (["ace_hearing"] call ace_common_fnc_isModLoaded && {_strength > 0} && { ace_hearing_damageCoefficent > 0.25}) then {
    private _earringingStrength = 40 * _strength;
    [_earringingStrength] call ace_hearing_fnc_earRinging;
};

// Add ace_medical pain effect
if ((missionNamespace getVariable ["ace_medical_enabled", false]) && {_strength > 0.1} && {isDamageAllowed _unit} && {_unit getVariable [ "ace_medical_allowDamage", true]}) then {
    [ACE_player, _strength / 2] call ace_medical_fnc_adjustPainLevel;
};

// Effect on vision has a wider range, with a higher falloff
_strength = 1 - (((_eyePos vectorDistance _grenadePosASL) min 25) / 25) ^ 0.4;
_strength = _strength * _losCoefficient;

// Account for people looking away by slightly reducing the effect for visual effects.
private _eyeDir = ((AGLtoASL positionCameraToWorld [0, 0, 1]) vectorDiff (AGLtoASL positionCameraToWorld [0, 0, 0]));
private _dirToUnitVector = _eyePos vectorFromTo _grenadePosASL;
private _angleDiff = acos (_eyeDir vectorDotProduct _dirToUnitVector);

// From 0-45deg, full effect
if (_angleDiff > 45) then {
    _strength = _strength * ((1 - (_angleDiff - 45) / (120 - 45)) max 0);
};

// Blind player
if (_strength > 0.1) then {
    private _blend = [[1, 1, 1, 0], [0.3, 0.3, 0.3, 1]] select ace_common_epilepsyFriendlyMode;

    ace_grenades_flashbangPPEffectCC ppEffectEnable true;
    ace_grenades_flashbangPPEffectCC ppEffectAdjust [1, 1, (0.8 + _strength) min 1, _blend, [0, 0, 0, 1], [0, 0, 0, 0]];
    ace_grenades_flashbangPPEffectCC ppEffectCommit 0.01;

    // PARTIALRECOVERY - start decreasing effect over time
    [{
        params ["_strength", "_blend"];

        ace_grenades_flashbangPPEffectCC ppEffectAdjust [1, 1, 0, _blend, [0, 0, 0, 1], [0, 0, 0, 0]];
        ace_grenades_flashbangPPEffectCC ppEffectCommit (10 * _strength);
    }, [_strength, _blend], 7 * _strength] call CBA_fnc_waitAndExecute;

    // FULLRECOVERY - end effect
    [{
        ace_grenades_flashbangPPEffectCC ppEffectEnable false;
    }, [], 17 * _strength] call CBA_fnc_waitAndExecute;
};

// Make player flinch
if (_strength <= 0.2) exitWith {};

private _minFlinch = linearConversion [0.2, 1, _strength, 0, 60, true];
private _maxFlinch = linearConversion [0.2, 1, _strength, 0, 95, true];
private _flinch    = (_minFlinch + random (_maxFlinch - _minFlinch)) * selectRandom [-1, 1];
ACE_player setDir (getDir ACE_player + _flinch);

["ace_grenades_flashbangedPlayer", [_strength, _grenadePosASL]] call CBA_fnc_localEvent;