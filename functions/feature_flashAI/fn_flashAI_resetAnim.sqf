/*
	MEX_fnc_flashAI_resetAnim
	Locality: Global effect
	Author: erem2k
	
    Resets flashbanged animation on a unit along with flashed state.

	Parameters:
        0 - Object, AI unit
            Flashbanged AI unit.

	Returns:
        None
*/

params [
    ["_unit", objNull, [objNull]]
];

if (isNull _unit) exitWith {};

if ([_unit] call ace_common_fnc_isPlayer) exitWith {};

if (not (_unit getVariable ["MEX_flashAI_isAnimationPlayed", false])) exitWith {};

// This will require custom animation to be set up in config
// For now we'll have to deal with immediate transition
private _animCfg = ["FlashAI", "Animations"] call TEX_fnc_cfg_get;

[
    {
        params["_unit", "_outMoves"];
        
        // Switch to first move in sequence, then play the rest
        {
            if (_forEachIndex == 0) then {
                
                [_unit, _x] remoteExec ["switchMove", 0];
                continue;
            };

            [_unit, _x] remoteExec ["playMoveNow", 0];

        } forEach _outMoves;

        _unit setUnitPos "UP";
    },
    [_unit, (_animCfg get "out")]
] call CBA_fnc_directCall;

// Reset flashed state once animation ends
if ([_unit] call ace_common_fnc_isAwake) then {
    _unit setVariable ["ace_grenades_flashReactionDebounce", 0];
};

_unit setVariable ["MEX_flashAI_isAnimationPlayed", false, true];