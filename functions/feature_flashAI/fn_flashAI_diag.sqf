/*
	MEX_fnc_flashAI_diag
    Locality: Local Effect
	Author: erem2k

	Attaches monitor event handler that outputs debug information about flashbanged AI every 2 seconds.

	Parameters:
		0 - Boolean, whether to attach event handler.

	Returns:
		None
*/

params[
    ["_enable", true, [true]]
];

private _existingEhId = missionNamespace getVariable ["MEX_fnc_flashAI_diag_monitorEh", nil];

// Disable monitor
if (not _enable) exitWith {

    if (isNil "_existingEhId") exitWith {};

    [
        "ace_grenades_flashbangedAI",
        _existingEhId
    ] call CBA_fnc_removeEventHandler;

    missionNamespace setVariable ["MEX_fnc_flashAI_diag_monitorEh", nil];
};

// Enable monitor
if (not (isNil "_existingEhId")) exitWith {};

private _newEhId = [
    "ace_grenades_flashbangedAI",
    {
        params["_unit", "_strength", "_grenadePosASL"];

        private _explosionCache = missionNamespace getVariable ["MEX_fnc_flashAI_diag_explosions", createHashMap];

        private _affectedUnits = _explosionCache getOrDefault [[_grenadePosASL, CBA_missionTime], []];
        _affectedUnits pushBack [_unit, _strength];

        _explosionCache set [[_grenadePosASL, CBA_missionTime], _affectedUnits];
        missionNamespace setVariable ["MEX_fnc_flashAI_diag_explosions", _explosionCache];

        private _isDelaying = missionNamespace getVariable ["MEX_fnc_flashAI_diag_isDelaying", false];

        if (_isDelaying) exitWith {};

        missionNamespace setVariable ["MEX_fnc_flashAI_diag_isDelaying", true];

        [
            {
                private _explosionCache = missionNamespace getVariable ["MEX_fnc_flashAI_diag_explosions", createHashMap];

                private _hintContents = [
                    "DIAG flashAI",
                    "\n",
                    "\n"
                ];

                private _logContents = [];

                {
                    private _explosionInfo = _x;
                    private _affectedUnits = _y;

                    _logContents append [
                        format ["Explosion at %1", _explosionInfo select 1],
                        "\n",
                        "    Unit#ID - Strength",
                        "\n"
                    ];

                    {
                        private _unit = _x param [0, objNull];
                        private _strength = _x param [1, -1];

                        _logContents pushBack (format ["    %1#%2 - %3\n", typeOf _unit, netId _unit, _strength]);

                    } forEach _affectedUnits;

                    _logContents pushBack "\n";

                } forEach _explosionCache;

                private _hintString = (_hintContents + _logContents) joinString "";
                private _logString = _logContents joinString "";

                hintSilent (_hintString);

                ["info", _logString] call TEX_fnc_log_all;

                missionNamespace setVariable ["MEX_fnc_flashAI_diag_explosions", nil];
                missionNamespace setVariable ["MEX_fnc_flashAI_diag_isDelaying", nil];
            },
            2.5
        ] call CBA_fnc_waitAndExecute;
    }
] call CBA_fnc_addEventHandler;

missionNamespace setVariable ["MEX_fnc_flashAI_diag_monitorEh", _newEhId];