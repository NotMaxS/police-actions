/*
	MEX_fnc_flashAI_onFlashbangedAI
	Locality: Global effect
	Author: erem2k
	
    Event callback for ACE AI flashbanged event.
        Forces AI into animation or briefly knocks them unconscious depending on strength of the flash.

	Parameters:
        0 - Object, AI unit
            Flashbanged AI unit.
        
        1 - Number, flashbang strength
            Ranges from 0.2 to 1.0
        
        3 - Array, position of flashbang
            Supplied in PositionASL format.

	Returns:
        None
*/

params[
    ["_unit", objNull, [objNull]], 
    ["_strength", 0, [0]], 
    ["_grenadePosASL", [], [[]], 3]
];

// Sanity checks, as they are not done in ACE
if (isNull _unit) exitWith {};
if (not alive _unit) exitWith {};

// Clamp flashbang duration to longest animation
private _globalCfg = ["FlashAI"] call TEX_fnc_cfg_get;
private _flashResetTime = _unit getVariable ["ace_grenades_flashReactionDebounce", 0];
private _flashResetDuration = (0) max (_flashResetTime - CBA_missiontime);

if (_flashResetDuration > (_globalCfg get "flashMaxDuration")) then {
    _unit setVariable ["ace_grenades_flashReactionDebounce", (CBA_missiontime + (_globalCfg get "flashMaxDuration"))];
};

// Apply effect based on perceived blast strength
private _thresholdCfg = ["FlashAI", "Thresholds"] call TEX_fnc_cfg_get;
private _isConscious = [_unit] call ace_common_fnc_isAwake;

if (_strength < (_thresholdCfg get "min")) exitWith {};

switch (true) do {
    case (_strength >= (_thresholdCfg get "knockout")):
    {
        if (!_isConscious) exitWith {};

        [_unit, true, random (_globalCfg get "knockoutTime"), true] call ace_medical_fnc_setUnconscious;
    };
    case (_strength >= (_thresholdCfg get "anim")):
    {
        if (!_isConscious) exitWith {};

        private _animRoll = random 1;

        if (_animRoll <= (_thresholdCfg get "animChance")) then {
            [_unit] call MEX_fnc_flashAI_playAnim;
        };
    };
};

// All thresholds receive LAMBS debuff as well
private _previousState = createHashMapFromArray [
    ["lambs_danger_disableAI", _unit getVariable ["lambs_danger_disableAI", false]],
    ["lambs_danger_dangerRadio", _unit getVariable ["lambs_danger_dangerRadio", false]]
];

_unit setVariable ["lambs_danger_disableAI", true, true];
_unit setVariable ["lambs_danger_dangerRadio", false, true];

// Reset it at the same time as skill reset occurs in ACE 
[
    {
        CBA_missiontime >= ((_this select 0) getVariable ["ace_grenades_flashReactionDebounce", 0])
    }, 
    {
        params ["_unit", "_previousState"];

        if (not (alive _unit)) exitWith {};

        {
            _unit setVariable [_x, _y, true];
        } forEach _previousState;
    },
    [_unit, _previousState]
] call CBA_fnc_waitUntilAndExecute;
