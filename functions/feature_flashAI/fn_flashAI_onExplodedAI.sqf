/*
	MEX_fnc_flashAI_onExplodedAI
	Locality: Global effect
	Author: KoffeinFlummi, erem2k
 
    KoffeinFlummi: Knocks out AI units.
	
    erem2k: Introduces additional LOS checks over ACE implementation.

	Parameters:
        0 - Array, Position ASL
            Flashbang explosion position.

	Returns:
        None
*/

params ["_grenadePosASL"];

private _checkedGrenadePos = +_grenadePosASL;
_checkedGrenadePos set [2, (_checkedGrenadePos select 2) + 0.2]; // compensate for grenade glitching into ground

private _affected = ((ASLtoAGL _checkedGrenadePos) nearEntities ["CAManBase", 20]) - [ACE_player];
private _subskills = [["FlashAI", "ACE", "aiSubskills"], []] call TEX_fnc_cfg_getOrDefault;

{
    private _unit = _x;

    ///////////////////////////////////////////////////////////////
    // LoS check
    private _eyePos = eyePos _unit; 

    private _losCount = {
        !lineIntersects [_checkedGrenadePos vectorAdd _x, _eyePos, _unit]
    } count [[0, 0, 0], [0, 0, 0.2], [0.1, 0.1, 0.1], [-0.1, -0.1, 0.1]];

    private _hasLos = _losCount > 0;
    private _isMinDistance = (_checkedGrenadePos vectorDistanceSqr _eyePos) <= 4.0; // 3^2

    if (!_hasLos and !_isMinDistance) then { continue; };

    ///////////////////////////////////////////////////////////////
    private _strength = 1 - (((eyePos _unit) vectorDistance _checkedGrenadePos) min 20) / 20;

    [_unit, true] call ace_common_fnc_disableAI;

    private _flashReactionDebounce = _unit getVariable ["ace_grenades_flashReactionDebounce", 0];
    _unit setVariable ["ace_grenades_flashReactionDebounce", _flashReactionDebounce max (CBA_missionTime + (7 * _strength))];

    // Skip if still flashed
    if (_flashReactionDebounce >= CBA_missionTime) then {
        continue;
    };

    ["MEX_flashAI_affectedUnitCount", 1] call TEX_fnc_var_inc;

    // Make AI try to look away - once, unlike ACE implementation
    private _dirToFlash = _unit getDir _checkedGrenadePos;
    _unit setDir (_dirToFlash + linearConversion [0.2, 1, _strength, 40, 135] * selectRandom [-1, 1]);

    // Invokes extended AI behavior
    _unit setVariable ["ace_grenades_flashStrength", _strength, true];
    ["ace_grenades_flashbangedAI", [_unit, _strength, _checkedGrenadePos]] call CBA_fnc_localEvent;

    {
        _unit setSkill [_x, (_unit skill _x) / 50];
    } forEach _subskills;

    [
        {
            CBA_missiontime >= _this getVariable ["ace_grenades_flashReactionDebounce", 0]
        }, 
        {
            params ["_unit"];

            _unit setVariable ["ace_grenades_flashStrength", 0, true];
            ["MEX_flashAI_affectedUnitCount"] call TEX_fnc_var_dec;

            // Make sure we don't enable AI for unconscious units
            if (_unit call ace_common_fnc_isAwake) then {
                [_unit, false] call ace_common_fnc_disableAI;
            };

            {
                _unit setSkill [_x, (_unit skill _x) * 50];
            } forEach ([["FlashAI", "ACE", "aiSubskills"], []] call TEX_fnc_cfg_getOrDefault);
        }, 
        _unit
    ] call CBA_fnc_waitUntilAndExecute;
} forEach (
    _affected select {
        local _x && {_x call ace_common_fnc_isAwake}
    }
);