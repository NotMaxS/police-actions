/*
	MEX_fnc_stats_onStatsUpdate
	Locality: Local effect, Server execution
	Author: erem2k
	
    Event callback for stats update event.
        Updates stats variables according to event type received.

	Parameters:
        0 - String, stats event type
            Expects enemyCaptured, hostageRescued, civilianKilled, playerKilled, playerWounded, enemyKilled

        1 - Number, amount of events of this type to process
            Number of times stats update event has occurred at the same time; multiplies applicable stat.

	Returns:
        None
*/

params[
    ["_eventType", "", [""]],
    ["_eventsCount", 1, [0]]
];

private _statMap = missionNamespace getVariable ["MEX_stats_currentStats", createHashMap];

_statMap set [_eventType, (_statMap getOrDefault [_eventType, 0]) + 1 * _eventsCount];

missionNamespace setVariable ["MEX_stats_currentStats", _statMap];