/*
	MEX_fnc_stats_getStats
	Locality: Local effect
	Author: erem2k
	
    Forms extended stats map based on current stats system state.

	Parameters:
        None

	Returns:
        None
*/


private _statsMap = missionNamespace getVariable ["MEX_stats_currentStats", createHashMap];

private _fn_getTotal = {

    params["_map", "_varName", "_cfgPath"];

    private _mapValue = _map getOrDefault [_varName, 0];
    private _configMultiplier = _cfgPath call TEX_fnc_cfg_get;

    _mapValue * _configMultiplier
};

createHashMapFromArray [
    [
        "hostageRescued", 
        _statsMap getOrDefault ["hostageRescued", 0]
    ],
    [
        "hostageRescuedTotal", 
        [_statsMap, "hostageRescued", ["Score", "Hostage", "bonusScore"]] call _fn_getTotal
    ],
    [
        "hostageKilled",
        _statsMap getOrDefault ["hostageKilled", 0]
    ],
    [
        "hostageKilledTotal", 
        [_statsMap, "hostageKilledTotal", ["Score", "Hostage", "bonusScore"]] call _fn_getTotal
    ],
    [
        "enemyCaptured", 
        _statsMap getOrDefault ["enemyCaptured", 0]
    ],
    [
        "enemyCapturedTotal", 
        [_statsMap, "enemyCaptured", ["Score", "Captive", "bonusScore"]] call _fn_getTotal
    ],
    [
        "enemyKilled",
        _statsMap getOrDefault ["enemyKilled", 0]        
    ],
    [
        "playerKilled", 
        _statsMap getOrDefault ["playerKilled", 0]
    ],
    [
        "playerWounded", 
        _statsMap getOrDefault ["playerWounded", 0]
    ],
    [
        "civilianKilled", 
        _statsMap getOrDefault ["civilianKilled", 0]
    ],
    [
        "civilianKilledTotal", 
        [_statsMap, "civilianKilled", ["Score", "Civilian", "penaltyScore"]] call _fn_getTotal
    ]
];
