/*
	MEX_fnc_stats_onAceKilled
	Locality: Local effect
	Author: erem2k
	
    Event callback for global ace_killed events.
        Updates stats accordingly if killed unit is of interest to any of these systems.

	Parameters:
        0 - Object, killed unit

        1 - String, cause of unit's death
            Set by ace_medical.
        
        2 - Object, unit's killer
            Can be none, vehicle or another unit.
        
        3 - Object, specific object that killed the unit.
            Specific player or AI that killed the unit; i.e. gunner in the vehicle that pulled the trigger, or last person to damage the bleeding unit.

	Returns:
        None
*/

params["_unit", "_causeOfDeath", "_killer", "_instigator"];

if (isNull _instigator) exitWith {};

private _isKilledByPlayer = [_instigator] call ace_common_fnc_isPlayer;
private _isKilledByBlufor = (side group _instigator) == west;

// General side bodycount handling
switch(side group _unit) do {
    case civilian:
    {
        // Killed civilians go under "Civilians killed" statistic
        if (!_isKilledByBlufor || !_isKilledByPlayer) exitWith {};
        
        ["MEX_stats_statsUpdate", ["civilianKilled", 1]] call CBA_fnc_serverEvent;
    };
    case west:
    {
        // Killed players go under "Officers killed" statistic
        if (!([_unit] call ace_common_fnc_isPlayer)) exitWith {};
        
        ["MEX_stats_statsUpdate", ["playerKilled", 1]] call CBA_fnc_serverEvent;
    };
    case east:
    {
        // Killed OPFOR go under "Hostiles killed" statistic
        if (!_isKilledByBlufor || !_isKilledByPlayer) exitWith {};

        ["MEX_stats_statsUpdate", ["enemyKilled", 1]] call CBA_fnc_serverEvent;
    };
};

// Special character handling
private _hostageUnits = ["Score", "Hostage", "eligibleUnits"] call TEX_fnc_cfg_get;

if ((typeOf _unit) in _hostageUnits) then {
    ["MEX_stats_statsUpdate", ["hostageKilled", 1]] call CBA_fnc_serverEvent;
};
