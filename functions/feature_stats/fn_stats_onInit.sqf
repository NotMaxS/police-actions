/*
	MEX_fnc_stats_onInit
	Locality: Local effect
	Author: NotMax
	
    Attaches event handlers required for function of stats module.

	Parameters:
        None

	Returns:
        None
*/

if (isServer) then {

    // Kitchen sink, processing all unit deaths on server
    // Tracks civilians, players and opfor AI kills; triggering stats updates
    [
        "ace_killed",
        {
            _this call MEX_fnc_stats_onAceKilled;
        }
    ] call CBA_fnc_addEventHandler;

    // Primary stats update callback
    [
        "MEX_stats_statsUpdate",
        {
            _this call MEX_fnc_stats_onStatsUpdate;
        }
    ] call CBA_fnc_addEventHandler;
};

// Track player wounded events with incapacitations
if (hasInterface) then {

    private _knockOutHandler = [
        "ace_medical_knockOut",
        {
            ["MEX_stats_statsUpdate", ["playerWounded", 1]] call CBA_fnc_serverEvent;
            
            ["ace_medical_knockOut", missionNamespace getVariable ["MEX_stats_playerKnockOutHandler", 0]] call CBA_fnc_removeEventHandler;
        }
    ] call CBA_fnc_addEventHandler;

    missionNamespace setVariable ["MEX_stats_playerKnockOutHandler", _knockOutHandler];
};