/*
	MEX_fnc_stats_diag
    Locality: Local Effect
	Author: erem2k

	Outputs debug information about stats module.
    Optionally can also attach per frame monitor of local variable state.

	Parameters:
		0 - Boolean, whether to show variable monitor hint.
			Passing False when monitor is being shown will disable it.

	Returns:
		String, current state of score variables on this client.
*/

params [["_attachMonitor", false]];

// Attach/remove per frame handler
private _existingDiagHandler = missionNamespace getVariable ["MEX_stats_diagPFH", nil];

if (isNil "_existingDiagHandler") then {

	if (_attachMonitor) then {
		
		private _diagHandler = [
            { 
                hintSilent (format ["DIAG stats\n%1", call MEX_fnc_stats_getStats]) 
            }, 2
        ] call CBA_fnc_addPerFrameHandler;

		missionNamespace setVariable ["MEX_stats_diagPFH", _diagHandler];
	};

} else {

	if (!_attachMonitor) then {
		[_existingDiagHandler] call CBA_fnc_removePerFrameHandler;
	};
};

// Finally, dump the varstate string
private _statState = call MEX_fnc_stats_getStats;
["info", "%1", _statState] call TEX_fnc_log_all;

_statState