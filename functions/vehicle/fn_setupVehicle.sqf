// SXP_fnc_setupVehicle
// Handles setting up vehicles at mission start, and upon respawn

// Only run on the server
if (!isServer) exitWith {};

// Define variables
_this params [
	["_newVeh", objNull, [objNull]],
	["_oldVeh", objNull, [objNull]],
	["_start", false, [true]]
];

// If newVeh is nil, exit the script
if (isNil "_newVeh") exitWith {};

// Start our switch block, check the vehicle classname to determine what needs to be done for setup
switch (toLower (typeOf _newVeh)) do {

	
	//BTR 80 
	case (toLower "CUP_B_BTR80_FIA"): {

		//load inventory 	
		[_newVeh, "myBTR"] call XPT_fnc_loadItemCargo;
		
		_newVeh disableNVGEquipment true;
	};

	//BTR 60 
	case (toLower "CUP_B_BTR60_CDF"): {

		//load inventory 	
		[_newVeh, "myBTR"] call XPT_fnc_loadItemCargo;
		
		_newVeh disableNVGEquipment true;

		// set up textures
		[_newVeh, "Police"] call MEX_fnc_vehicleTexture_apply;

		// activate jankmobile
		[_newVeh, "FlashLauncher"] call MEX_fnc_vehicleComposition_create;
	};

	//Infantry car
	case (toLower "CUP_LADA_LM_CIV"): {

		//load inventory 	
		[_newVeh, "mySquadCar"] call XPT_fnc_loadItemCargo;
	};

	//Amberlance
	case (toLower "CUP_B_S1203_Ambulance_CDF"): {

		//load inventory 	
		[_newVeh, "myAmbulance"] call XPT_fnc_loadItemCargo;
	};

	//Sniper vehicle
	case (toLower "CUP_B_UAZ_Unarmed_AFU"): {

		//load inventory 	
		[_newVeh, "myUAZ"] call XPT_fnc_loadItemCargo;
	};

	//Command vehicle
	case (toLower "CUP_C_Lada_CIV"): {

		//load inventory 	
		[_newVeh, "myCommandCar"] call XPT_fnc_loadItemCargo;
			
	};
		
	//Jouranlist Vehicle
	case (toLower "CUP_C_Volha_CR_CIV"): {

		//load inventory 	
			[_newVeh, "myJouranlistCar"] call XPT_fnc_loadItemCargo;
			
	};

	case (toLower "CUP_O_V3S_Rearm_TKA"): 
	{
		//AP Magazines for BTR KPVT
		for "_myAPMagNumber" from 0 to 15 do {
		
		[_newVeh, "CUP_50Rnd_TE2_LRT4_Green_Tracer_145x115_KPVT_M"] call ace_rearm_fnc_addMagazineToSupply;
		}; 

		//HE Magazines for BTR KPVT
		for "_myHEMagNumber" from 0 to 3 do {
		
		[_newVeh, "CUP_50Rnd_TE2_LRT4_Green_Tracer_145x115_KPVT_MDZ_M"] call ace_rearm_fnc_addMagazineToSupply;
		}; 

		//MG Magazines for BTR PKT
		for "_myMGMagNumber" from 0 to 11 do {
		
		[_newVeh, "CUP_250Rnd_TE1_Green_Tracer_762x54_PKT_M"] call ace_rearm_fnc_addMagazineToSupply;
		}; 

		//Magazine for the AGS to rearm 
		[_newVeh, "CUP_29Rnd_30mm_AGS30_M"] call ace_rearm_fnc_addMagazineToSupply;
		[_newVeh, "CUP_29Rnd_30mm_AGS30_M"] call ace_rearm_fnc_addMagazineToSupply;

	}; 
};