/*
	MEX_fnc_ambientAnim_aiDisable
	Locality: Local Effect
	Author: erem2k
	
    Disables unit's AI so it can be animated.
        Caches previous AI settings values to be restored when animation ends.

	Parameters:
        0 - Object, AI unit 
            Unit playing ambient animation started by MEX_fnc_ambientAnim_start

	Returns:
        None
*/


params[
    ["_unit", objNull, [objNull]]
];

if (isNull _unit) exitWith {};

// Helper function definitions
private _fn_disableBisFeature = {
    params["_unit", "_feature"];

    private _wasEnabled = _unit checkAIFeature _feature;

    _unit disableAI _feature;

    _wasEnabled
};

private _fn_disableLambsFeature = {
    params["_unit", "_feature"];

    private _oldValue = _unit getVariable _feature;

    private _newValue = switch(_feature) do {
        case "lambs_danger_disableAI":
        {
            true
        };
        case "lambs_danger_dangerRadio": // i love consistent interfaces
        {
            false
        };
        default
        {
            ["(disableLambsFeature) Trying to handle disable request for unknown feature %1, setting to false", _feature] call TEX_fnc_log;
            false
        };
    };

    _unit setVariable [_feature, _newValue];

    // Propagate nil if variable didn't exist
    if (isNil "_oldValue") then { nil } else { _oldValue };
};

///////////////////////
private _toggleFeatures = ["AmbientAnimations", "toggleAiFeatures"] call TEX_fnc_cfg_get;
private _unitStateCache = createHashMap;

// This needs to be invoked on every machine, because disableAI is local - so it needs to be set everywhere
// Having it on all machines should cover rare cases of unit switching locality during ongoing animation and it breaking
{ 
	
    private _featureName = _x;
    private _featureCachedState = false;

    switch (true) do {

        case ("lambs_danger_" in _featureName):
        {
            _featureCachedState = [_unit, _featureName] call _fn_disableLambsFeature;
        };
        default
        {
            _featureCachedState = [_unit, _featureName] call _fn_disableBisFeature;
        };
    };

    if (isNil "_featureCachedState") then { continue; };

    _unitStateCache set [_featureName, _featureCachedState];

} forEach _toggleFeatures;

_unit setVariable ["MEX_ambientAnimation_aiStateCache", _unitStateCache];