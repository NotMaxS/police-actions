/*
	MEX_fnc_ambientAnim_onPreInit
	Locality: Local Effect
	Author: erem2k
	
    Attaches event handlers for ambientAnim module on mission preinit.

	Parameters:
        None

	Returns:
        None
*/

[
    "MEX_ambientAnim_Animate",
    {
        _this spawn { _this call MEX_fnc_ambientAnim_onAnimate; };
    }
] call CBA_fnc_addEventHandler;

[
    "MEX_ambientAnim_toggleAi",
    {
        _this spawn { _this call MEX_fnc_ambientAnim_onToggleAi; };
    }
] call CBA_fnc_addEventHandler;