/*
	MEX_fnc_ambientAnim_switchNextMove
	Locality: Global Effect
	Author: erem2k
    
    Plays next animation on unit from the current animation set.

	Parameters:
        0 - Object, AI unit 
            Unit playing ambient animation started by MEX_fnc_ambientAnim_start

	Returns:
        None
*/



params[
    ["_animatedUnit", objNull, [objNull]]
];

if (isNull _animatedUnit) exitWith {};

// Assigned in MEX_fnc_ambientAnim_start
private _animSetName = _animatedUnit getVariable ["MEX_ambientAnim_currentSet", ""];

if (_animSetName == "") exitWith {
    ["error", "Unit [%1; %2] has no animation set assigned!", _animatedUnit, netId _animatedUnit] call TEX_fnc_log;
};

// Select a random anim from the pool of available animations and play it
private _selectedMove = selectRandom (["AmbientAnimations", _animSetName, "moves"] call TEX_fnc_cfg_get);

[_animatedUnit, _selectedMove] remoteExec ["switchMove", 0];