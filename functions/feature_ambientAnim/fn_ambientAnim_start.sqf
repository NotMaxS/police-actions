/*
	MEX_fnc_ambientAnim_start
	Locality: Global Effect
	Author: erem2k
	
    Starts execution of looped ambient animation for provided unit.
        Optionally animation can be made interruptible if unit enters combat.

        Heavily based on Achilles mod implementation, that in turn mimics BIS_fnc_ambientAnim/BIS_fnc_ambientAnimCombat.

	Parameters:
        0 - Object, AI unit 
            Unit that will play the requested animation. 

        1 - String, animation set name
            Name of animation set defined in AmbientAnimations.hpp config.

        2 - Bool, should the animation be cancelled when entering combat
            If True, will also trigger when units gets damaged or spots an enemy.

	Returns:
        None
*/

params[
    ["_unit", objNull, [objNull]], 
    ["_animSetName", "GUARD", [""]], 
    ["_reactToCombat", false, [false]]
];

// Unit must be local to the client attaching the animation
if (not local _unit) exitWith {};

private _isSwitchingAnimation = false;

// Terminate previous animation
if (!isNil {_unit getVariable "MEX_ambientAnim_currentSet"}) then
{
	[_unit] call MEX_fnc_ambientAnim_stop;
	_isSwitchingAnimation = true;
};

// Fetch set info
private _animSetInfo = ["AmbientAnimations", _animSetName] call TEX_fnc_cfg_get;
private _animHidesWeapon = _animSetInfo get "hideWeapon";

if (isNil "_animSetInfo") exitWith {
	["error", "Unknown animation %1 requested, aborting", _animSetName] call TEX_fnc_log;
};

_unit setVariable ["MEX_ambientAnim_currentSet", _animSetName, true];
_unit setVariable ["MEX_ambientAnim_isWeaponHidden", _animHidesWeapon, true];

// Turn AI brain off
["MEX_ambientAnim_toggleAi", [_unit, false]] call CBA_fnc_globalEvent;

// Hide weapon if requested by animation set
if (_animHidesWeapon) then
{
	private _weapon = primaryWeapon _unit;

	if (!_isSwitchingAnimation) then {
		_unit setVariable ["MEX_ambientAnim_unitWeapon", _weapon, true]
	};

	_unit removeWeapon _weapon;
};

// Push animation start request event
["MEX_ambientAnim_Animate", [_unit, _reactToCombat]] call CBA_fnc_localEvent;
