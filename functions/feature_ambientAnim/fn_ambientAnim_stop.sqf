/*
	MEX_fnc_ambientAnim_stop
	Locality: Global Effect
	Author: erem2k
	
    Stops execution of looped ambient animation for provided unit.
        Unit must be local to calling machine.

	Parameters:
        0 - Object, AI unit 
            Unit playing ambient animation started by MEX_fnc_ambientAnim_start

	Returns:
        None
*/

params[["_unit", objNull, [objNull]]];

if (not local _unit) exitWith {
    ["error", "Trying to stop ambient animation for non-local unit!"] call TEX_fnc_log;
};

if (isNull _unit) exitWith {};

// Tear down event handlers
private _handlerId = _unit getVariable ["MEX_ambientAnim_handlerAnimDone", 0];
_unit removeEventHandler ["AnimDone", _handlerId];

_handlerId = _unit getVariable ["MEX_ambientAnim_handlerKilled", 0];
_unit removeEventHandler ["Killed", _handlerId];

_handlerId = _unit getVariable ["MEX_ambientAnim_handlerKnockOut", 0];
["ace_medical_knockOut", _handlerId] call CBA_fnc_removeEventHandler;

// Reset to default animation
_unit playMoveNow "AmovPercMstpSrasWrflDnon";
_unit setUnitPos "UP";

_unit setVariable ["MEX_ambientAnim_currentSet", nil, true];
_unit setVariable ["MEX_ambientAnim_isWeaponHidden", nil, true];

// Reassign the weapon back from shadow dimension
private _isWeaponHidden = _unit getVariable ["MEX_ambientAnim_isWeaponHidden",false];

if (_isWeaponHidden) then
{
    private _weapon = _unit getVariable ["MEX_ambientAnim_unitWeapon",""];
    _unit addWeapon _weapon;
    _unit selectWeapon _weapon;
};

// Turn AI brain on
["MEX_ambientAnim_toggleAi", [_unit, true]] call CBA_fnc_globalEvent;
