/*
	MEX_fnc_ambientAnim_aiEnable
	Locality: Local Effect
	Author: erem2k
	
    Enables unit's AI so it can resume typical AI activities after animation ends.
        Each AI feature will be reset to aiStateCache-supplied state, and if it could not be found - it will just be enabled.

	Parameters:
        0 - Object, AI unit 
            Unit that played ambient animation started by MEX_fnc_ambientAnim_start

	Returns:
        None
*/


params[
    ["_unit", objNull, [objNull]]
];

if (isNull _unit) exitWith {};

// Helper function definitions
private _fn_enableBisFeature = {
    params["_unit", "_feature", "_cache"];

    private _wasEnabled = _cache getOrDefault [_feature, true];

    if (_wasEnabled) then {
        _unit enableAI _feature;
    } else {
        _unit disableAI _feature;
    };
};

private _fn_enableLambsFeature = {
    params["_unit", "_feature", "_cache"];

    private _cachedValue = _cache get _feature;

    // Means feature wasn't set at all before it was disabled, do the same
    if (isNil "_featureValue") exitWith {
        _unit setVariable [_feature, nil];
    };

    _unit setVariable [_feature, _cachedValue];
};

///////////////////////
private _toggleFeatures = ["AmbientAnimations", "toggleAiFeatures"] call TEX_fnc_cfg_get;
private _unitStateCache = _unit getVariable ["MEX_ambientAnimation_aiStateCache", createHashMap];

// Same deal as with aiDisable counterpart, we enable requrested AI functions locally on every machine to cover changing locality
// God forbid BIS actually propagates AI logic over network, that would be too reasonable
{ 
	
    private _featureName = _x;

    switch (true) do {

        case ("lambs_danger_" in _featureName):
        {
            [_unit, _featureName, _unitStateCache] call _fn_enableLambsFeature;
        };
        default
        {
            [_unit, _featureName, _unitStateCache] call _fn_enableBisFeature;
        };
    };

} forEach _toggleFeatures;

_unit setVariable ["MEX_ambientAnimation_aiStateCache", nil];