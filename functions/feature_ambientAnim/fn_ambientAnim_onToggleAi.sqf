/*
	MEX_fnc_ambientAnim_onToggleAi
	Locality: Local Effect
	Author: erem2k
	
    Event callback responding to local AI toggle requests.

	Parameters:
        0 - Object, AI unit 
            Unit playing ambient animation started by MEX_fnc_ambientAnim_start
        
        1 - Boolean, whether AI should be enabled for the unit

	Returns:
        None
*/


params [
    ["_unit", objNull, [objNull]],
    ["_doEnable", false, [false]]
];

if (isNull _unit) exitWith {};

if (_doEnable) then {
    [_unit] call MEX_fnc_ambientAnim_aiEnable;
} else {
    [_unit] call MEX_fnc_ambientAnim_aiDisable;
};
