/*
	MEX_fnc_ambientAnim_onAnimate
	Locality: Global Effect
	Author: erem2k
	
    Sets up animation loop to be played on passed unit, and plays initial animation.
        This is achieved by event handlers that will appropriately loop or stop played animation on unit state changes.
    
    Internal use, start animation with MEX_fnc_ambientAnim_start instead.

	Parameters:
        0 - Object, AI unit 
            Unit that will play the requested animation. 

        2 - Bool, should the animation be cancelled when entering combat
            If True, will also trigger when units gets damaged or spots an enemy.

	Returns:
        None
*/

params[
    ["_animatedUnit", objNull, [objNull]],
    ["_reactToCombat", false, [true]]
];

// Init safeguard
waitUntil{time > 0};

if (isNil "_animatedUnit") exitWith {};
if (isNull _animatedUnit) exitWith {};
if !(alive _animatedUnit && canMove _animatedUnit) exitWith {};

// Bust a move
[_animatedUnit] call MEX_fnc_ambientAnim_switchNextMove;

// ...and loop it until unil unit dies, gets knocked out, or optionally gets alerted via combat
private _handlerId = _animatedUnit addEventHandler [
    "AnimDone",
    {
        params ["_animatedUnit"];

        if (alive _animatedUnit) then
        {
            [_animatedUnit] call MEX_fnc_ambientAnim_switchNextMove;
        }
        else
        {
            [_animatedUnit] call MEX_fnc_ambientAnim_stop;
        };
    }
];

_animatedUnit setVariable ["MEX_ambientAnim_handlerAnimDone", _handlerId, true];

_handlerId = _animatedUnit addEventHandler [
    "Killed",
    {
        (_this select 0) call MEX_fnc_ambientAnim_stop;
    }
];

_animatedUnit setVariable ["MEX_ambientAnim_handlerKilled", _handlerId, true];

_handlerId = [
    "ace_medical_knockOut",
    {
        params["_unit"];
        [_unit] call MEX_fnc_ambientAnim_stop;
    }
] call CBA_fnc_addEventHandler;

_animatedUnit setVariable ["MEX_ambientAnim_handlerKnockOut", _handlerId, true];

// Finally, handle the teardown logic for units that should react to combat
if (!_reactToCombat) exitWith {};

private _initialDamage = damage _animatedUnit;
[
    {
        params["_animatedUnit", "_unitInitialDamage"];
        (behaviour _animatedUnit == "COMBAT") || (damage _animatedUnit > _unitInitialDamage) || (_animatedUnit call BIS_fnc_enemyDetected) || !(alive _animatedUnit) || (_animatedUnit getVariable["ACE_isUnconscious", false])
    },
    {
        params["_animatedUnit", "_unitInitialDamage"];

        // Already handled by ace_medical_knockOut EH
        if (_animatedUnit getVariable["ACE_isUnconscious", false]) exitWith {};

        [_animatedUnit] call MEX_fnc_ambientAnim_stop;
    },
    [_animatedUnit, _initialDamage]
] call CBA_fnc_waitUntilAndExecute;