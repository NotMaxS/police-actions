/*
	MEX_fnc_vehicleTexture_apply
	Locality: Global Effect
	Author: erem2k
	
    Applies mission config-defined custom texture to the vehicle.
        Texture can be supplied both from mission or addon path.

	Parameters:
        0 - Object, vehicle 
            Vehicle texture will be applied to.
        
        1 - String, texture set name (optional)
            Select specific texture set to apply when vehicle has multiple defined.

	Returns:
        None
*/

params[
    ["_vehicle", objNull, [objNull]],
    ["_setName", "", [""]]
];

if (isNull _vehicle) exitWith {};

// Check if if even have it defined first
private _rootTextureInfo = ["VehicleTextures", typeOf _vehicle] call TEX_fnc_cfg_get;

if (isNil "_rootTextureInfo") exitWith {
    [
        "error", 
        "Trying to apply texture to vehicle %1 with no configuration defined!", 
        typeOf _vehicle
    ] call TEX_fnc_log;
};

// Then, process requested texture set info - fall back on root set if none are defined
private _setTextureInfo = createHashMap;

if (_setName != "") then {

    _setTextureInfo = [
        _rootTextureInfo getOrDefault [_setName, configNull],
        createHashMap
    ] call TEX_fnc_cfg_getOrDefault;
};

if (count (_setTextureInfo) <= 0) then {
    _setTextureInfo = _rootTextureInfo;
};

// Finally, apply the texture and materials
{
    private _selectionId = _forEachIndex;
    private _texturePath = _x;

    _vehicle setObjectTextureGlobal [_selectionId, _texturePath];

} forEach (_setTextureInfo getOrDefault ["hiddenSelectionTextures", []]);

{
    private _selectionId = _forEachIndex;
    private _materialPath = _x;

    _vehicle setObjectMaterialGlobal [_selectionId, _materialPath];

} forEach (_setTextureInfo getOrDefault ["hiddenSelectionMaterials", []]);
