/*
	MEX_fnc_vehicleTexture_applyRandom
	Locality: Global Effect
	Author: erem2k
	
    Applies random mission config-defined custom texture to the vehicle.
        Texture can be supplied both from mission or addon path.

        Textures that don't have weight defined are considered to weigh 1.0 by default.

	Parameters:
        0 - Object, vehicle 
            Vehicle texture will be applied to.
        
        1 - Boolean, whether to include root config in random pool

	Returns:
        None
*/

params[
    ["_vehicle", objNull, [objNull]],
    ["_includeRootSet", true, [false]]
];

if (isNull _vehicle) exitWith {};

// Pull vehicle config
private _rootTextureInfo = ["VehicleTextures", typeOf _vehicle] call TEX_fnc_cfg_get;

if (isNil "_rootTextureInfo") exitWith {
    [
        "error", 
        "Trying to apply random texture to vehicle %1 with no configuration defined!", 
        typeOf _vehicle
    ] call TEX_fnc_log;
};

// Build input for selectRandomWeighted
private _textureSetNames = [];
private _textureSetWeights = [];

if (_includeRootSet) then {
    _textureSetNames pushBack nil;
    _textureSetWeights pushBack (_rootTextureInfo getOrDefault["weight", 1.0])
};

{
    private _keyName = _x;
    private _keyValue = _y;

    if (!(_keyValue isKindOf configNull)) then {
        continue;
    };

    private _setTextureInfo = [_keyValue, createHashMap] call TEX_fnc_cfg_getOrDefault;

    _textureSetNames pushBack _keyName;
    _textureSetWeights pushBack (_setTextureInfo getOrDefault ["weight", 1.0]);

} forEach _rootTextureInfo;

// Finally, pick the random set and apply the texture
[_vehicle, _textureSetNames selectRandomWeighted _textureSetWeights] call MEX_fnc_vehicleTexture_apply;