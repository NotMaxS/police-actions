/*
	SLV_fnc_score_handleInitWagon
	Locality: Local effect, Server execution
	Author: NotMax, erem2k
	
    Attaches event handlers to captive drop-off vehicle.
        Called from InitPost XEH, these handlers will update mission score and stats accordingly.

	Parameters:
        0 - Object, vehicle where captives will be dropped off.
            Handles both hostages and hostile captives.

	Returns:
        None
*/

params ["_dropOffWagon"]; 

if (!isServer) exitWith {};

_dropOffWagon addEventHandler [
    "GetIn", 
    {
	    params ["_vehicle", "_role", "_unit", "_turret"];

        private _captureableUnits = ["Score", "Captive", "eligibleUnits"] call TEX_fnc_cfg_get;
        private _hostageUnits = ["Score", "Hostage", "eligibleUnits"] call TEX_fnc_cfg_get;

        if ((alive _unit) && ((typeOf _unit) in _captureableUnits)) then 
        {
            ["SLV_score_scoreUpdate", ["enemyCaptured", 1]] call CBA_fnc_serverEvent;
            ["MEX_stats_statsUpdate", ["enemyCaptured", 1]] call CBA_fnc_serverEvent;

            deleteVehicle _unit; 
        }; 

        if ((alive _unit) && (typeOf _unit == "CUP_B_CDF_Officer_FST")) then 
        {
            ["SLV_score_scoreUpdate", ["hostageRescued", 1]] call CBA_fnc_serverEvent;
            ["MEX_stats_statsUpdate", ["hostageRescued", 1]] call CBA_fnc_serverEvent;

            deleteVehicle _unit;  
        };
    }
];
