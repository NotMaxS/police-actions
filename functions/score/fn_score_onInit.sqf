/*
	SLV_fnc_score_onInit
	Locality: Local effect, Server execution
	Author: NotMax, erem2k
	
    Sets up event listeners and variables required for function of score module.
        Called on preInit.

	Parameters:
        None

	Returns:
        None
*/


if (!isServer) exitWith {};

SLV_playerScoreVar = 0; 
missionNamespace setVariable ["SLV_playerScoreVar", 0, true]; 

// Kitchen sink, processing all unit deaths on server
// Tracks civilians, players and opfor AI kills; triggers score update if civilian is killed by players.
[
    "ace_killed",
    {
        _this call SLV_fnc_score_onAceKilled;
    }
 ] call CBA_fnc_addEventHandler;

// Handles score update events, adding or subtracting it
[
    "SLV_score_scoreUpdate", 
    {_this call SLV_fnc_score_onScoreUpdate;}
] call CBA_fnc_addEventHandler;
