/*
	SLV_fnc_score_getEnding
	Locality: Local effect
	Author: NotMax, erem2k
	
    Returns ending type based on current player team score.

	Parameters:
        None

	Returns:
        Array of two elements, String and Bool
            String corresponds to ending type in CfgDebriefing, Bool indicates whether ending is considered victory.
*/

private _endPlayerScore = missionNameSpace getVariable ["SLV_playerScoreVar", 0];

private _endingType = "";
private _isVictory = false;

switch (true) do {

    case (_endPlayerScore >= 1000): 
    {
        _endingType = "totalVictory";
        _isVictory = true;
    };
    
    case (_endPlayerScore >= 750): 
    {
        _endingType = "regularVictory";
        _isVictory = true;
    };

    case (_endPlayerScore >= 600): 
    {
        _endingType = "partialVictory";
        _isVictory = true;
    };

    case (_endPlayerScore < 600): 
    {
        _endingType = "youLost";
        _isVictory = false;
    };

};

[_endingType, _isVictory]