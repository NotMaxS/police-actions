/*
	SLV_fnc_score_diag
    Locality: Local Effect
	Author: erem2k

	Outputs debug information about score module.
    Optionally can also attach per frame monitor of local variable state.

	Parameters:
		0 - Boolean, whether to show variable monitor hint.
			Passing False when monitor is being shown will disable it.

	Returns:
		String, current state of score variables on this client.
*/

params [["_attachMonitor", false]];

// Fetch and stringify local variable state
private _fn_varsToString = {

	private _scoreState = str (missionNamespace getVariable ["SLV_playerScoreVar", 0]);
	private _endingType = str ([] call SLV_fnc_score_getEnding);

	format ["Score:\n%1\n\Current ending:\n%2", _scoreState, _endingType];
};

// Attach/remove per frame handler
private _existingDiagHandler = missionNamespace getVariable ["SLV_score_diagPFG", nil];

if (isNil "_existingDiagHandler") then {

	if (_attachMonitor) then {
		
		private _diagHandler = [{ hintSilent (format ["DIAG itemSet\n%1", call (_this select 0)]) }, 2, _fn_varsToString] call CBA_fnc_addPerFrameHandler;
		missionNamespace setVariable ["SLV_score_diagPFG", _diagHandler];
	};

} else {

	if (!_attachMonitor) then {
		[_existingDiagHandler] call CBA_fnc_removePerFrameHandler;
	};
};

// Finally, dump the varstate string
private _varState = call _fn_varsToString;
["info", "%1", _varState] call TEX_fnc_log_all;

_varState