/*
	SLV_fnc_score_onScoreUpdate
	Locality: Global effect
	Author: erem2k
	
    Processes score update events and applies them to current score value.

	Parameters:
        0 - String, score update event type
            Currently supported: enemyCaptured, hostageRescued, civilianKilled

        1 - Number, amount of events of this type to process
            Number of times score update event has occurred at the same time; multiplies score bonus or penalty.

	Returns:
        None
*/

params[
    ["_eventType", "", [""]],
    ["_eventCount", 1, [0]]
];

if (!isServer) exitWith {};

private _multiplier = switch (_eventType) do {
    case "enemyCaptured":
    {
        ["Score", "Captive", "bonusScore"] call TEX_fnc_cfg_get
    };
    case "hostageRescued":
    {
        ["Score", "Hostage", "bonusScore"] call TEX_fnc_cfg_get
    };
    case "civilianKilled":
    {
        ["SLV_score_penaltyApplied", ["civilianKilled"]] call CBA_fnc_globalEvent;

        -(["Score", "Civilian", "penaltyScore"] call TEX_fnc_cfg_get) // <-- Negative!
    };
    default
    {
        ["error", "Unknown event type: %1, no change to score has been made!", _eventType] call TEX_fnc_log;
        nil
    };
};

if (isNil "_multiplier") exitWith {};

["SLV_playerScoreVar", _multiplier * _eventCount, 0, true] call TEX_fnc_var_inc;