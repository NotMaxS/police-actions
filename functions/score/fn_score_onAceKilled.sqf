/*
	SLV_fnc_score_onAceKilled
	Locality: Global effect
	Author: erem2k
	
    Event callback for global ace_killed events.
        Updates score accordingly if killed unit is of interest to any of these systems.

	Parameters:
        0 - Object, killed unit

        1 - String, cause of unit's death
            Set by ace_medical.
        
        2 - Object, unit's killer
            Can be none, vehicle or another unit.
        
        3 - Object, specific object that killed the unit.
            Specific player or AI that killed the unit; i.e. gunner in the vehicle that pulled the trigger, or last person to damage the bleeding unit.

	Returns:
        None
*/

params["_unit", "_causeOfDeath", "_killer", "_instigator"];

if (isNull _unit) exitWith {};

if ((side group _unit) != civilian) exitWith {};

// For now we're interested only in civilian kills to penalize the score
if ((side _instigator) == west && ([_instigator] call ace_common_fnc_isPlayer)) then {
    ["SLV_score_scoreUpdate", ["civilianKilled", 1]] call CBA_fnc_globalEvent;
};
