/*
	SLV_fnc_mission_getEndSummary
	Locality: Local effect
	Author: erem2k
	
    Builds and returns "Operation summary" debriefing page based on values of stats and score modules.

	Parameters:
        0 - String, ending type
            Must correspond to ending types defined in CfgDebriefing.

	Returns:
        Structured text, contains debriefing page
*/

params[["_endingType", "", [""]]];


private _completedWith = switch (_endingType) do {

    case "totalVictory": { "total CERS victory."; };
    
    case "regularVictory": { "CERS victory." };

    case "partialVictory": { "partial CERS victory." };    
    
    case "youLost": { "CERS failing to achieve any meaningful progress." };
    
    default { "unknown ending type! pretend you didn't see it" };
};


private _scoreInfo = [] call MEX_fnc_stats_getStats;

private _bonusesHeading = text "Bonuses:";
_bonusesHeading setAttributes ["font", "PuristaBold"];

private _penaltiesHeading = text "Penalties:";
_penaltiesHeading setAttributes ["font", "PuristaBold"];

private _lossesHeading = text "Losses:";
_lossesHeading setAttributes ["font", "PuristaBold"];

// Debriefing UI does not support stylized text, because why would any user-facing system do that :)
[
    "Operation completed with ",
    _completedWith,
    "<br/>",
    "<br/>",
    "<t font='RobotoCondensedBold'>",
    _bonusesHeading,
    "</t>",
    "<br/>",
    format[" - Hostages rescued: %1 (+%2 pts)", _scoreInfo get "hostageRescued", _scoreInfo get "hostageRescuedTotal"],
    "<br/>",
    format[" - Hostiles captured: %1 (+%2 pts)", _scoreInfo get "enemyCaptured", _scoreInfo get "enemyCapturedTotal"],
    "<br/>",
    "<br/>",
    "<t font='RobotoCondensedBold'>",
    _penaltiesHeading,
    "</t>",
    "<br/>",
    format[" - Hostages killed: %1 (voided %2 pts)", _scoreInfo get "hostageKilled", _scoreInfo get "hostageKilledTotal"],
    "<br/>",
    format[" - Civilians killed: %1 (-%2 pts)", _scoreInfo get "civilianKilled", _scoreInfo get "civilianKilledTotal"],
    "<br/>",
    "<br/>",
    "<t font='RobotoCondensedBold'>",
    _lossesHeading,
    "</t>",
    "<br/>",
    "ChDKZ:",
    "<br/>",
    format[" - Killed: %1", _scoreInfo get "enemyKilled"],
    "<br/>",
    "CERS:",
    "<br/>",
    format[" - Killed: %1",  _scoreInfo get "playerKilled"],
    "<br/>",
    format[" - Wounded: %1", _scoreInfo get "playerWounded"],
    "<br/>",
    "<br/>",
    format["Final CERS score: <t font='RobotoCondensedBold'> %1 pts</t>", missionNamespace getVariable ["SLV_playerScoreVar", 0]]
] joinString "";
