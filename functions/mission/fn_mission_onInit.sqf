/*
	SLV_fnc_mission_onInit
	Locality: Local effect, Server execution
	Author: NotMax, erem2k
	
        Sets up event listeners and variables required for function of mission module.
                Called on preInit.

	Parameters:
        None

	Returns:
        None
*/

if (!isServer) exitWith {};

// Collects information from score and stat systems, triggers mission end event
[
    "SLV_MissionEndEvent", 
    {[] call SLV_fnc_mission_onMissionEnd;}
] call CBA_fnc_addEventHandler;

