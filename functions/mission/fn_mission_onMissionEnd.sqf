/*
        SLV_fnc_mission_onMissionEnd
        Locality: Global effect
        Author: erem2k

        Called back on mission end request event.
                Polls stat and score systems, prepares and triggers correct mission ending.

        Parameters:
        None

        Returns:
        None
*/


([] call SLV_fnc_score_getEnding) params [
    "_endingType",
    "_isVictory"
];

private _endingSummary = [_endingType] call SLV_fnc_mission_getEndSummary;
missionNamespace setVariable ["SLV_mission_endSummary", _endingSummary, true];

// Pull the plug
[_endingType, _isVictory, true, true] remoteExec ["BIS_fnc_endMission", 0, true];
