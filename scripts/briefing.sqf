// Script to handle initial mission briefings
// General guidelines would be to include briefings for the following
// Situation, Mission, and Assets
// Briefings are listed in the opposite order that they are written below. New diaryRecords are always placed at the top of the list.
// https://community.bistudio.com/wiki/createDiaryRecord

/*
	===== TO CREATE A BASIC BRIEFING =====
	The following code will add a "basic" briefing to all units in the mission

	player createDiaryRecord ["Diary", ["Assets", "Example Mission Assets"]];
	player createDiaryRecord ["Diary", ["Mission", "Example Mission Briefing"]];
	player createDiaryRecord ["Diary", ["Situation", "Example Situation Briefing"]];


	===== TO CREATE A SIDE-SPECIFIC BRIEFING =====
	The following code will add a briefing *only* to a certain side
	In this example, a briefing will be created that is only visible to BLUFOR players
	UNLESS YOUR MISSION HAS MULTIPLE PLAYER SIDES. YOU DO NOT NEED THIS CODE.
	
	if ((side player) == west) then {
		player createDiaryRecord ["Diary", ["Mission", "BLUFOR mission notes go here"]];
	};
	
	
	===== TO CREATE A ZEUS-SPECIFIC BRIEFING =====
	The following code will add a briefing *only* to player zeus units.
	
	if (player isKindOf "VirtualCurator_F")then {
		player createDiaryRecord ["Diary", ["Zeus Notes", "Zeus notes go here"]];
	};
	
	
	===== NOTES =====
	Keep in mind that even with these if-statements, briefings will still appear in *reverse order from which they are written*
	This means if you want an extra note for a specific side that goes at the bottom of the briefing, that briefing should go at the top of this file.
*/

if (player isKindOf "VirtualCurator_F")then {
		player createDiaryRecord ["Diary", ["Zeus Notes", 
		"In this mission, you will be in charge of both baby sitting the AI and make sure they are responsive to players as the majority
		of this op is CQB. It will be very important for you to make sure that AI groups inside buildings stay in the STANDING position
		so as to avoid them shooting through floors.<br/> 
		Use any Chernarussian Movement of the Red Star unit, they have custom randomized loadouts. The only AT they will have will be
		the RPG-18s. Only spawn the machine gun Hilux's and keep them to a low amount.<br/>
		If you plan to spawn any civilians, only use the BASE civilian class. Civilian/civilians/men/civilian aka C_man_1. They will also
		have randomized loadouts.<br/>
		If you want the ambient civilian groups to go prone in response to nearby firefights, you will need to take them off careless.
		The units performing static animations won't react to stance commands even off-careless,
		 I'd suggest using the Uncon module on them to make them go down. If you Heal them afterwards, they will not return to their animation state.<br/> 
		If the reporters misbehave or abuse their civilian status, kill them. They will not respawn."
		]];
};

if (side player == west) then 
{
player createDiaryRecord ["Diary", ["Assets", "Asset List:<br/>
	- 3x VAZ-2103 Police Cruisers<br/>
	- 1x UAZ Unarmed - Sniper Vehicle<br/>
	- 2x Skoda S1203 Paramedic Vans<br/>
	- 1x BTR-80<br/>
	- 1x BTR-60 - Special Breaching Operations Variant
"]];
}; 

if (side player == civilian) then 
{
player createDiaryRecord ["Diary", ["Assets", "Asset List:<br/>
	- 1x GAZ-24 Volga"]];
}; 


if (side player == west) then
{
player createDiaryRecord ["Diary", ["Intel Enemy",
"Enemies are expected to be using various small arms including 9mm, shotguns, 5.45 and 7.62 weaponry. They will be using grenades 
and some might be equipped with flashbangs. They will have limited AT  capabilities, primarily single shot disposables,
and have a limited supply of machine gun technicals. They will primarily be holding up inside buildings and not risk 
fighting in the open."
]];
}; 

if (side player == west) then 
{
player createDiaryRecord ["Diary", ["Task Intel",
"Zone 1:<br/> 
According to reports from local police forces, the insurgents will be holed up inside the police station and the school house just 
opposite to it. The hostages will be the Police Chief and Assistant Police Chief. They will be dressed in CDF Camo. 
Enemy technicals were sighted at this location.<br/><br/> 
Zone 2:<br/> 
Insurgents will be holding up inside the factory complex, primarily in the main factory buildings.The hostages at this location are believed
to be the two factory supervisors and will be dressed in construction worker high-vis. Enemy technicals were sighted
at this location.<br/><br/> 
Zone 3:<br/>
The Chedaki in this zone will be holding up inside grey apartement complexes according to intel. Hostages in this zone are believed 
to be local police officers. They will be dressed the same as those at Zone 1. A pro-Movement of the Red Star
rally is happening in the neighbouring residential zone, watch your fire.<br/><br/>
Zone 4:<br/> 
The CHDKZ will be barricaded inside the central buildings of the two city blocks that this zone encompasses. They have taken the head
priest from the central church and the mayor of the city hostage at this location. A protest is happening
nearby at the central square and at the central square church, watch your fire.<br/><br/> 
Zone 5:<br/>
The Chedaki have taken over the city's Grand Hotel and the nearby Central Hospital. They have taken a French Diplomat who was staying
at the hotel hostage along with the chief doctor of the Hospital."
]];
}; 

if (side player == west) then 
{
player createDiaryRecord ["Diary", ["Victory Conditions",
"In order to achieve victory conditions, a certain number of points must be accumulated. Points will be awarded to players when
a hostage or captured insurgent are loaded into the Ural Transport Truck at the Hostage and Prisoner Collection Point. Points
will be deducted when a unarmed civilian is killed. Score can be viewed at any time in the mission by pressing the 'N' key.<br/><br/>
Point distribution:<br/>
Rescued hostage +100 pts (note, killing a hostage does not deduct points)<br/> 
Captured insurgent +50 pts<br/>
Killed civilian -100pts (note, this only applies to civilians killed by players).<br/><br/> 
Score requirements:<br/> 
Perfect - 1000pts<br/> 
Good - 750pts<br/> 
Bare minimum - 600pts<br/>
Anything less will result in a loss.<br/><br/>

To end the mission at anytime, complete the hold action on the radio at the Hostage and Prisoner Collection Point."
]];
}; 

if (side player == west) then 
{
player createDiaryRecord ["Diary", ["Mission",
"With the local police force unequipped to handle the situation and the bulk of the CDF tied up elsewhere, a ragtag taskforce of
Chernarus Emergency Response Security operators and local paramedics has been assembled to spearhead the police operation in Susice. 
The goal of the Emergency Response Forces will be to liberate hostages in key zones, neutralize or arrest insurgents and minimize
civilian deaths. While local pro-Russian protestors and agitators are present on the streets, they will be dealt
with by follow up local police forces during cleanup operations.<br/><br/> 
-Radios per teams will be limited. <br/> 
-Breach teams will be equipped with zip ties and flashbangs for capturing insurgents<br/> 
-Navigation will be handled with ACE Dagrs, use your Ace-self interact menu to toggle it<br/>
-In addition to the indicated civilian concentration, civilians may be hiding inside buildings that you will be clearing. Watch your fire<br/>
-Looting of enemy caches and ammunition is permitted, local police forces will handle their destruction later<br/>"
]];
}; 

if (side player == civilian) then 
{
	player createDiaryRecord ["Diary", ["Reporter Mission", 
	"The Chernarussian Times have sent you two to document the police response to the insurgency within the town of Susice.
	As part of the ongoing constitutional crisis, our goal is to document any potential police abuse of peaceful protestors and
	mishandling of hostages or arrested individuals. Stay safe and take lots of pictures. Don't do anything stupid that would 
	result in your arrest by the police."]]; 
}; 

player createDiaryRecord ["Diary", ["Situation",
"In this alternate timeline where the Chernarussian Movement of the Red Star came into being as early as 1992, a constitutional crisis
sweeps the nation of Chernarus which has just started to enjoy its independence from the Soviet Union. Profiting from the chaos, 
the CHDKZ have launched multiple attacks across the country, stretching the Chernarus Defence Forces and the Chernarus Police thin. 
In the border Oblast of Sumova, the Chedaki have pushed the local police force out of the main city of Susice and barricaded themselves in multiple key areas. 
Taking multiple hostages, they await Russian Federation Forces, which have amassed a 'peacekeeping' taskforce on the border, 
to intervene. Together with local pro-Russian protestors, they hope to have the border Oblast be 'liberated' by the Russians."
]];
