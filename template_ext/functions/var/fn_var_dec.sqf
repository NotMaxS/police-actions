/*
	TEX_fnc_var_dec
	Locality: Local Effect
	Author: erem2k
	
    Decrements value in selected namespace, optionally broadcasting it.
        Helper function to wrap over variable operations.

	Parameters:
        0 - A) Variant (Mission namespace variable)
            0 - String, variable name
                Assumed variable namespace with this syntax is missionNamespace.

        0 - B) Variant (Custom namespace variable)
            0 - Array of two elements, String and Namespace
                Specifies custom namespace where to set the variable; also supports objects and CBA namespaces.

        1 - Any, decrement value (optional)
            Subtracted to variable's current value; defaults to 1.
        
        2 - Any, default value (optional)
            Variable value in case it was not found in provided namespace; defaults to 0
        
        3 - Boolean, whether variable value will be broadcast (optional)
            Broadcast will occur through setVariable if specified

	Returns:
        Any, new variable value
*/


params[
    ["_variable", nil, ["", []], [2]],
    ["_increment", 1],
    ["_defaultValue", 0],
    ["_isBroadcast", false, [true]]
];

if (isNil "_variable") exitWith {};

private _varName = _variable;
private _namespace = missionNamespace;

if (_variable isEqualType []) then {
    _varName = _variable param[0, nil, [""]];
    _namespace = _variable param[1, nil, [missionNamespace, objNull]];
};

if (isNil "_varName") exitWith {};

private _newValue = (_namespace getVariable [_varName, _defaultValue]) - _increment;
_namespace setVariable [_varName, _newValue, _isBroadcast];

_newValue