/*
	TEX_fnc_log_server
	Locality: Global Effect
	Author: erem2k
	
    Wrapper over template's XPT_fnc_log that automatically formats input string with passed variables.
        Helper variant that emits passed log string only on server machine.

	Parameters:
		0 - String or Number, log severity.

            As per XPT_fnc_log documentation:
                0: "Error" - Errors that will impact the operation of a system, but not the entire mission.
		        1: "Warning" - Issues that may result in undesired effects, but will not break systems.
		        2: "Info" - Information regarding the operation of the template.
		        3: "Debug" - Debug information. Not logged unless debug mode is enabled
    
        1 - A) Variant (Log string):
            1 - String, log string
                Freeform log string, can optionally include positional references to variables.

        1 - B) Variant (Log and module name strings):
            1 - Array, of 2x String
                First is log module name, second is log string.

        2 .. N - Any or None, variables referenced by log string.
            Referred to in log string by format identifiers, i.e. %1, %2 etc.

	Returns:
        None
*/

private _logLevel = _this param [0, 2, [0, ""]];
private _logContent = _this param [1, nil, ["", []], [2]];

// If user does not explicitly override this, force calling module name
if (!(_logContent isEqualType [])) then {
    _logContent = [_fnc_scriptNameParent, _logContent, 1];
};

[_logLevel, _logContent, _this select [2]] call TEX_fnc_log;