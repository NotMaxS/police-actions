/*
	TEX_fnc_log
	Locality: Local Effect
	Author: erem2k
	
    Wrapper over template's XPT_fnc_log that automatically formats input string with passed variables.

	Parameters:
		0 - String or Number, log severity.
        
            As per XPT_fnc_log documentation:
                0: "Error" - Errors that will impact the operation of a system, but not the entire mission.
		        1: "Warning" - Issues that may result in undesired effects, but will not break systems.
		        2: "Info" - Information regarding the operation of the template.
		        3: "Debug" - Debug information. Not logged unless debug mode is enabled
    
        1 - A) Variant (Log string):
            1 - String, log string
                Freeform log string, can optionally include positional references to variables.

        1 - B) Variant (Log and module name strings):
            1 - Array, of 2x String
                First is log module name, second is log string.

        1 - C) Variant (Log, module name string and locality value):
            1 - Array, of 2x String and Number
                First string is log module name, second string is log string, third number is locality.

                As per XPT_fnc_log documentation:
                    0: (local) - Will only log the error on the machine upon which the error occured.
                    1: (server) - Will log the error on the machine, as well as the server.
                    2: (all) - Will log the error on all connected machines.

        2 .. N - Any or None, variables referenced by log string.
            Referred to in log string by format identifiers, i.e. %1, %2 etc.

	Returns:
        None
*/

private _logLevel = _this param [0, 2, [0, ""]];
private _logContent = _this param [1, nil, ["", []], [2, 3]];

if (isNil "_logContent") exitWith {};

private _formatInput = [];
private _loggedModule = _fnc_scriptNameParent;
private _logLocality = 0;

// Special case for ["ModuleName", "LogString", Locality] log input
if (_logContent isEqualType []) then {

    _loggedModule = _logContent param [0, "<NO MODULE>", [""]];
    _formatInput pushBack (_logContent param [1, "", [""]]);
    _logLocality = _logContent param [2, 0, [0]];
} else {
    _formatInput pushBack _logContent;
};

// Can't really use flatten here, as it will crush nested arrays too
{
    _formatInput pushBack _x;
} forEach (_this select [2]);

// Finally, submit the formatted string to XPT log
[_logLevel, [_loggedModule, format _formatInput], _logLocality] call XPT_fnc_log;
