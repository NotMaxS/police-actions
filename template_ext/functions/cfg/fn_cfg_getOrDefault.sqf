/*
	TEX_fnc_cfg_getOrDefault
	Locality: Local Effect
	Author: erem2k
	
	Returns mission config value or class located at passed path.
        If value or class could not be found, user-provided default value is returned.

        Accepts both String-based and Config-based paths.

	Parameters (String syntax):
		0 - Array of String, path to config class or member key.
            i.e. ["MyCategory", "MyClass", "MyValue"] 

        1 - Any, user-provided default value.
            Returned when config class or member was not found.

    Parameters (Config syntax):
        0 - Config, config-type path to class or member key.
            i.e. configFile >> "MyCategory" >> "MyClass" >> "MyValue"
        
        1 - Any, user-provided default value.
            Returned when config class or member was not found.
	
	Returns:
		Any, or default value when config class or value is not found.
            Classes are returned as hash maps, where nested classes are represented as Config type.
*/

params["_configPath", "_defaultValue"];

private _configValue = _configPath call TEX_fnc_cfg_get;

if (isNil "_configValue") exitWith {
    _defaultValue
};

_configValue