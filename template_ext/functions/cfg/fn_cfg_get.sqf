/*
	TEX_fnc_cfg_get
	Locality: Local Effect
	Author: erem2k
	
	Returns mission config value or class located at passed path.
        If value or class could not be found, nil is returned.

        Accepts both String-based and Config-based paths.

	Parameters (String syntax):
		0 ... N - String, path to config class or member key.
            i.e. ["MyCategory", "MyClass", "MyValue"] 

    Parameters (Config syntax):
        0 - Config, config-type path to class or member key.
            i.e. configFile >> "MyCategory" >> "MyClass" >> "MyValue"
	
	Returns:
		Any, or nil when config class or value is not found.
            Classes are returned as hash maps, where nested classes are represented as Config type.
*/

if (count _this <= 0) exitWith { nil };

private _configPath = configNull;
private _fn_postProcessFieldValue = {

    if (count _this == 0) exitWith { nil };

    // Pass through all data types but strings
    if (not ((_this select 0) isEqualType "")) exitWith { _this select 0 };

    // getCfgData deserializes booleans into strings because Arma
    // So we have two options here - it's a legit string, or boolean pretending to be one
    switch (_this select 0) do {
        case "True";
        case "true": {
            true
        };
        case "False";
        case "false": {
            false
        };

        default {
            _this select 0
        };
    };
};

// Support for both config and string syntax 
if (_this isEqualType configFile) then {
    _configPath = _this;
} else {
    _configPath = flatten [missionConfigFile, "CfgTEX", "MissionConfig", _this];
};

// Early out if requested key refers to class member field
if (!(_configPath call BIS_fnc_getCfgIsClass)) exitWith {
    [_configPath call BIS_fnc_getCfgData] call _fn_postProcessFieldValue;
};

// Fill up hash table for requested class
private _configInstance = _configPath call BIS_fnc_getCfg;
private _configClassValue = createHashMap;

{
    private _fieldConfig = _x;
    private _fieldName = configName _x;

    // Only leave config references for nested classes
    if (isClass _fieldConfig) then {

        _configClassValue set [_fieldName, _fieldConfig];
        continue;
    };

    _configClassValue set [
        _fieldName,
        [_fieldConfig call BIS_fnc_getCfgData] call _fn_postProcessFieldValue
    ];
} foreach (configProperties [_configInstance]);

_configClassValue