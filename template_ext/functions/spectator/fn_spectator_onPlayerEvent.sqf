/*
	TEX_fnc_spectator_onPlayerEvent
	Locality: Local Effect
	Author: erem2k
	
    Wraps over ACE spectator, also toggling TFAR spectator mode on unit.
        Called back from TEX_Spectator respawn template after player is killed or respawned.

    While TFAR mode is supposed to be enabled by ACE inside it's setSpectator code, that doesn't seem to work properly.

	Parameters:
        0 - Object, killed or respawned player
            Former is passed on onPlayerKilled, latter on onPlayerRespawn respectively.

        1 - Object, player's killer or old player unit
            Former is passed on onPlayerKilled, latter on onPlayerRespawn respectively.

        2 - Number, respawn type index
            Corresponds to base game respawn type IDs; ACE spectator supports only "BIRD", "INSTANT", "BASE", "GROUP", "SIDE"

        3 - Number, respawn delay
            Seconds till respawn

	Returns:
        None
*/

_this call ace_spectator_fnc_respawnTemplate;

private _unit = param [0, objNull, [objNull]];

if (isNull _unit) exitWith {};

// Special case, spectators always have TFAR enabled
if (_unit isKindOf "ace_spectator_virtual") exitWith {
    _unit setVariable ["TFAR_forceSpectator", true];
};

_unit setVariable ["TFAR_forceSpectator", playerRespawnTime > 1];