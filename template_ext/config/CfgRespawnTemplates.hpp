// CfgRespawnTemplates.hpp

/*
    Wraps over ACE spectator, fixing TFAR compatibility.
*/
class TEX_Spectator
{
    onPlayerKilled = "TEX_fnc_spectator_onPlayerEvent";
    onPlayerRespawn = "TEX_fnc_spectator_onPlayerEvent";

    respawnTypes[] = {1,2,3,4,5};
};