class TEX
{
    class cfg
    {
        file = "template_ext\functions\cfg";
        class cfg_get {};
        class cfg_getOrDefault {};
    };

    class log
    {
        file = "template_ext\functions\log";
        class log {};
        class log_all {};
        class log_local {};
        class log_server {};
    };

    class spectator
    {
        file = "template_ext\functions\spectator";
        class spectator_onPlayerEvent {};
    };

    class var
    {
        file = "template_ext\functions\var";
        class var_dec {};
        class var_inc {};
    };
};